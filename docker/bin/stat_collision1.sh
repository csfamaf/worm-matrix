#!/bin/bash

# set -x

if [ $# -lt 3 ]
then
    echo "$0 needs two arguments" >&2
    echo "$0 <max_dist_collision> <nrun> <linear_speed_factor> [more params]" >&2
    exit 1
fi

max_dist_collision=$1
nrun=$2
linear_speed_factor=$3

shift 3

NRCERO=$(printf "%03d" $nrun)
MDCCERO=$(printf "%04d" $max_dist_collision)
COLLISION_RESULT_FILE="collision_result.$MDCCERO.$NRCERO.$linear_speed_factor.csv"


[ -a "$collision_RESULT_FILE" ] \
    && mv "$COLLISION_RESULT_FILE" "$COLLISION_RESULT_FILE.old"

# rm $STAT_RANGE_RESULT_FILE
cd ~/worm-matrix
. install/setup.bash

# https://answers.gazebosim.org//question/21103/exception-sending-a-message/
export IGN_IP=127.0.0.1

ros2 launch worm_connectome collision.launch.py \
        "nrun:=$nrun" "collision_result_file:=$COLLISION_RESULT_FILE" \
        "max_dist_collision:=$max_dist_collision" \
        "linear_speed_factor:=$linear_speed_factor" \
        $@

