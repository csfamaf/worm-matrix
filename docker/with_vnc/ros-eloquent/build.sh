#!/bin/bash

set -x

IM_VERSION=1.0

CONTAINER=ros-eloquent

IM=c-elegans/$CONTAINER:$IM_VERSION

IM_WORM_MATRIX=c-elegans/worm-matrix:$IM_VERSION

if [ -n "$(docker images -q $IM 2>/dev/null)" ]
then
    docker container rm ros-eloquent-worm-matrix
    docker image rm $IM_WORM_MATRIX
    docker container rm $CONTAINER
    docker image rm $IM
fi

docker build --tag $IM --build-arg UID=$UID -f dockerfiles/Dockerfile context

