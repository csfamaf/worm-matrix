#!/bin/bash

set -x

IM=c-elegans/worm-matrix:1.0

if [ -n "$(docker images -q $IM 2>/dev/null)" ]
then
    docker container rm ros-eloquent-worm-matrix
    docker image rm $IM
fi

docker build --tag $IM --build-arg UID=$UID -f dockerfiles/Dockerfile .


