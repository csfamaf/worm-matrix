#!/bin/bash

echoerr() { echo "$@" 1>&2; }

prog_exists() {
    if ! command -v $1 &> /dev/null
    then
        echoerr $1 cannot be found
        exit 1
    fi
}

for prg in realpath docker
do
    prog_exists $prg
done

DIRNAME="$(dirname "$0")"

HOST_DIR="$(realpath "$DIRNAME")"

exec docker run -it -p 6080:80 -v "$HOST_DIR"/..:/home/ubuntu/worm-matrix --name ros-eloquent-worm-matrix c-elegans/worm-matrix:1.0

