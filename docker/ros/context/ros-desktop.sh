#!/usr/bin/env bash
set -eu

# This install script is based on https://github.com/Tiryoh/ros2_setup_scripts_ubuntu, released under the Apache-2.0.

# REF: https://index.ros.org/doc/ros2/Installation/Linux-Install-Debians/
# by Open Robotics, licensed under CC-BY-4.0
# source: https://github.com/ros2/ros2_documentation

CHOOSE_ROS_DISTRO=foxy
CHOOSE_GAZEBO_VERSION=11 # According http://gazebosim.org/tutorials/?tut=ros_wrapper_versions
INSTALL_PACKAGE=desktop

curl -Ls https://raw.githubusercontent.com/ros/rosdistro/master/ros.key | apt-key add -
sh -c 'echo "deb http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" > /etc/apt/sources.list.d/ros2-latest.list'

# Install last gazebo 11
sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu-stable `lsb_release -cs` main" > /etc/apt/sources.list.d/gazebo-stable.list'
curl -Ls https://packages.osrfoundation.org/gazebo.key | apt-key add -

apt-get update -q
apt-get install -y ros-${CHOOSE_ROS_DISTRO}-${INSTALL_PACKAGE}
apt-get install -y python3-argcomplete
apt-get install -y python3-colcon-common-extensions
apt-get install -y python3-rosdep python3-vcstool # https://index.ros.org/doc/ros2/Installation/Linux-Development-Setup/
rosdep init
rosdep update

apt-get install -y gazebo${CHOOSE_GAZEBO_VERSION}

apt-get install -y ros-${CHOOSE_ROS_DISTRO}-gazebo-ros-pkgs


grep -F "source /opt/ros/${CHOOSE_ROS_DISTRO}/setup.bash"/etc/skel/.bashrc ||
echo "source /opt/ros/${CHOOSE_ROS_DISTRO}/setup.bash" >> /etc/skel/.bashrc

set +u

source /opt/ros/${CHOOSE_ROS_DISTRO}/setup.bash

echo "success installing ROS2 ${CHOOSE_ROS_DISTRO}"
echo "Run 'source /opt/ros/${CHOOSE_ROS_DISTRO}/setup.bash'"
