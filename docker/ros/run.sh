#!/bin/bash

set -x

CHOOSE_ROS_DISTRO=foxy

DIRNAME="$(dirname "$0")"
GIT_DIR="$(realpath "$DIRNAME")"/../..

docker run -it --rm \
    -v "$GIT_DIR":/worm-matrix\
    --hostname worm \
    --name ros-${CHOOSE_ROS_DISTRO} \
    c-elegans/ros-${CHOOSE_ROS_DISTRO}:1.0 /bin/bash
