#!/bin/bash

set -x

IM_VERSION=1.0

CHOOSE_ROS_DISTRO=foxy

CONTAINER=ros-$CHOOSE_ROS_DISTRO

IM=c-elegans/$CONTAINER:$IM_VERSION

CONTAINER_WORM=$CONTAINER-worm-matrix

IM_WORM_MATRIX=c-elegans/$CONTAINER_WORM:$IM_VERSION

if [ -n "$(docker images -q $IM 2>/dev/null)" ]
then
    docker container rm $CONTAINER_WORM
    docker image rm $IM_WORM_MATRIX
    docker container rm $CONTAINER
    docker image rm $IM
fi

docker build --tag $IM  -f dockerfiles/Dockerfile context

