#!/usr/bin/env bash
set -x

# Create user

MYUSER=ubuntu
MYGROUP=ubuntu
HOME=/home/$MYUSER

MYUID="$(id -u)"
MYGID="$(id -g)"

test_writable() {

    if ! [ -w $1 ] ; then
	echo "Cannot modify $1 file:"
	ls -l $1
	exit 1
    fi

}

test_writable /etc/sudoers
test_writable /etc/passwd
test_writable /etc/shadow

echo "$MYUSER ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

# create anonymous user to use sudo
echo "$MYUSER:x:$MYUID:$MYGID:anonymous uid:$HOME:/bin/false" >> /etc/passwd
echo $MYUSER':*:18663:0:99999:7:::' >> /etc/shadow

sudo chmod 644 /etc/passwd
sudo chmod 440 /etc/sudoers
sudo chmod 640 /etc/shadow
sudo chgrp shadow /etc/shadow

# Modify create user with usermod
sudo usermod --home $HOME --shell /bin/bash --groups adm,sudo $MYUSER

# Create home if not exist
if ! [ -d $HOME ] 
then 
    sudo cp -R /etc/skel $HOME
    sudo chown -R $MYUID:$MYGID $HOME
fi

# Link to git if imported
[ -d /worm-matrix ] && ln -s /worm-matrix $HOME/worm-matrix

echo cd >> $HOME/.bashrc
echo cd worm-matrix >> $HOME/.bashrc

if [ "$1" == "--pause" ]
then
    shift
    set +x
    echo -n "Press Enter to continue..."
    read
    set -x

fi

sudo gosu $MYUSER "$@"
#sudo gosu $MYUSER /usr/bin/screen -s /bin/bash
