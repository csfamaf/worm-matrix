#!/bin/bash

set -x

IM_VERSION=1.0

CHOOSE_ROS_DISTRO=foxy

CONTAINER=ros-${CHOOSE_ROS_DISTRO}-worm-matrix

IM=c-elegans/$CONTAINER:$IM_VERSION

if [ -n "$(docker images -q $IM 2>/dev/null)" ]
then
    docker container rm $CONTAINER
    docker image rm $IM
fi

docker build --tag $IM  -f dockerfiles/Dockerfile context

