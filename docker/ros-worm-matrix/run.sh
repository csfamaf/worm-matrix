#!/bin/bash

#set -x

MYUID=$(id -u)

DIRNAME="$(dirname "$0")"
GIT_DIR="$(realpath "$DIRNAME")"/../..

CHOOSE_ROS_DISTRO=foxy

CONTAINER=ros-$CHOOSE_ROS_DISTRO

IM_VERSION=1.0

if [ $# == 0 ]
then
    CONTAINER_NAME=${CONTAINER}-worm-matrix
    docker run -it --rm -u $MYUID \
        -v "$GIT_DIR":/worm-matrix\
        --hostname worm \
        --name $CONTAINER_NAME \
        c-elegans/${CONTAINER}-worm-matrix:${IM_VERSION} \
        --pause /usr/bin/screen -s /bin/bash
else # Run command in dettached mode
    if [ $1 == --network ]
    then
        PARAMS="--network worm$2"
        #PARAMS="--network worm$2 --expose $((11345+$2))"
        shift 2
    else
        PARAMS=""
    fi
    CONTAINER_NAME=$(echo $@ | tr " /:=" _)
    docker run -d --rm -u $MYUID \
        -v "$GIT_DIR":/worm-matrix\
        --hostname worm \
        --name $CONTAINER_NAME \
        $PARAMS \
        c-elegans/${CONTAINER}-worm-matrix:${IM_VERSION} \
        "$@"
fi
