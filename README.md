# Worm in the matrix

Based in https://github.com/Connectome/GoPiGo/blob/master/GoPiGoConnectome.py

## Directory structure

* Base programs: `src/worm_connectome/worm_connectome/`.
* Statistical results: `stats/`
* sdf worlds (empty, playground, ...): `worlds/`
* sdf models (two wheel car, ...): `models/`

## Install and run

0. Follow [Installing ROS 2 Eloquent Elusor](https://index.ros.org/doc/ros2/Installation/Eloquent/) guide 
to install [ROS2](https://www.ros.org/).

0. Install [Gazebo](http://gazebosim.org)
    - Ubuntu/Debian:  
        ```
        sudo apt install ros-eloquent-gazebo-ros-pkgs
        ```
   - From source: [Installing gazebo_ros_pkgs](http://gazebosim.org/tutorials?tut=ros2_installing&cat=connect_ros)
   
0. Clone repo:   
    ```
    git clone https://<bitbucket user>@bitbucket.org/csfamaf/worm-matrix.git
    ```  
    or using ssh:  
    ```
    git clone git@bitbucket.org:csfamaf/worm-matrix.git
    ```
    
0. Compile:  
    ```
    cd worm-matrix
    ```  
    ```
    colcon build  
    ```

0. Install environment:  
    ```
    . install/setup.bash
    ```

0. Run simulation:  
    ```
   ros2 launch worm_connectome worm_connectome_playground2.launch.py
    ```

# Notes

## Timothy muscle lists error 1

* The original Timothy program has an error in the muscle list. 
    - muscleBodyList: MDL21 MVL21 where duplicates and there weren't any MDR21 MVR21
    - mBodyRight: there were MDL21 MVL21 but not MDR21 MVL21
    - musBodyDright: there was MDL21 but not MDR21
    - musBodyVright: there was MVL21 but not MVR21

    See comment in `src/worm_connectome/worm_connectome/synaptics.py` for the correction.
    
* The fix of this was made in `src/worm_connectome/worm_connectome/synaptics.py` at `e7c2e8ac6a0a0c0fc369bc810642f5dcfc18894a` commit.

* With this fixes `run_disembodied` run faster (from 10343 to 1809 seconds).

## Timothy muscle lists error 2

* There aren't MDL24 MDR24 MVR24 in the original program muscle list (worm doesn't have MVL24).

* These were aded in '31e5e14dcd7452a848614bcb1471b04883ccd6f3'.

