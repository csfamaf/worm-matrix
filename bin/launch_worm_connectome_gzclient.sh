#/bin/bash

. install/setup.bash

[ $? == 0 ] || exit

exec ros2 launch worm_connectome worm_connectome.launch.py gzclient:=True "$@"
