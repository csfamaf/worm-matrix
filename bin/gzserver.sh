#!/bin/bash

set -x 

DIRNAME="$(dirname "$0")"

PLUGIN_BASE_DIR="$DIRNAME/../build"

PLUGIN_DIRS="$PLUGIN_BASE_DIR/gazebo_ros_ghost_drive/:$PLUGIN_BASE_DIR/my_diff_drive/:$PLUGIN_BASE_DIR/gazebo_ros_diff_head_drive/:$PLUGIN_BASE_DIR/my_ray_sensor/"

GAZEBO_PLUGIN_PATH="$PLUGIN_DIRS" gzserver --verbose "$@"
