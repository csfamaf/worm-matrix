#!/bin/bash

set -x

NRUNS_START=${NRUNS_START:-0} # Default 0
NRUNS=${NRUNS:-100}

RANDOM_INIT=${RANDOM_INIT:-yes}

MAX_TIME_SEC=${MAX_TIME_SEC:-1800}

{ set +x; } 2>/dev/null # Temporary remove debug trace

echo -n "Press Enter to continue"
read

set -x

for nrun in $(seq $NRUNS_START $(($NRUNS_START+$NRUNS-1)))
do

    NRCERO=$(printf "%03d" $nrun)

    FILEO_PATH="odom_disembodied.$NRCERO.csv"
    FILEO_CELLS="cells_disembodied.$NRCERO.csv"

    ros2 run worm_connectome disembodied $FILEO_PATH $FILEO_CELLS --random-init yes --seed $nrun --max-time-sec $MAX_TIME_SEC

done