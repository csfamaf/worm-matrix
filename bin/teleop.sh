#!/bin/bash

set -x 
DIRNAME="$(dirname "$0")"

cd "$DIRNAME/.."

. install/setup.bash

./bin/gzserver.sh worlds/corridor.world >/dev/null 2>&1 &

PID_GZSERVER=$$

ros2 run worm_connectome teleop

kill $PID_GZSERVER
