#!/bin/bash

# set -x

kill_zombies() {

    while pkill -u $UID gzclient
    do
        sleep 1
    done

    while pkill -u $UID gzserver
    do
        sleep 1
    done

    RE_PCONNECTOME="python.*worm_connectome/connectome.*__node:="
    RE_PSTAT="python.*food_experiment/stat_collision.*$COLLISION_RESULT_FILE.*__node:="


    while pkill -u $UID -n -f $RE_PCONNECTOME
    do
        sleep 1
    done

    while pkill -u $UID -n -f $RE_PSTAT
    do
        sleep 1
    done
}

nrun=$1
NRCERO=$(printf "%03d" $nrun)
COLLISION_RESULT_FILE="collision_result.$NRCERO.csv"


[ -a "$collision_RESULT_FILE" ] \
    && mv "$COLLISION_RESULT_FILE" "$COLLISION_RESULT_FILE.old"

# rm $STAT_RANGE_RESULT_FILE

. install/setup.bash

# https://answers.gazebosim.org//question/21103/exception-sending-a-message/
export IGN_IP=127.0.0.1

kill_zombies

sleep 1

ros2 launch worm_connectome collision.launch.py \
        "nrun:=$nrun" "collision_result_file:=$COLLISION_RESULT_FILE" # "max_time:=1800"

RES=$?

sleep 1

kill_zombies

exit $RES

