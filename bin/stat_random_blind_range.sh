#!/bin/bash

NRUNS=15

DIRNAME="$(dirname "$0")"

#[ -a "$STAT_RANGE_RESULT_FILE" ] \
#    && mv "$STAT_RANGE_RESULT_FILE" "$STAT_RANGE_RESULT_FILE.old"

# rm $STAT_RANGE_RESULT_FILE

. install/setup.bash

# https://answers.gazebosim.org//question/21103/exception-sending-a-message/
export IGN_IP=127.0.0.1

for nrun in $(seq 0 $[$NRUNS-1])
do
    "$DIRNAME/stat_random_blind_range1.sh" $nrun

    [ $? == 0 ] || break
done
