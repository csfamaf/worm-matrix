#!/bin/bash

DIRNAME="$(dirname "$0")"

cd "$DIRNAME/.."

. install/setup.bash

exec ros2 launch worm_connectome worm_connectome_corridor.launch.py
