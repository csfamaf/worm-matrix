#!/bin/bash

NRUNS=100

DIRNAME="$(dirname "$0")"

for nrun in $(seq 0 $[$NRUNS-1])
# for nrun in $(seq 50 99)
do
    "$DIRNAME/stat_collision1.sh" $nrun

    [ $? == 0 ] || break
done
