#!/bin/bash

# set -x

kill_zombies() {

    while pkill -u $UID gzclient
    do
        sleep 1
    done

    while pkill -u $UID gzserver
    do
        sleep 1
    done

    RE_PCONNECTOME="python.*food_experiment/gradient.*__node:="
    RE_PSTAT="python.*food_experiment/odom.*$STAT_RANGE_RESULT_FILE.*__node:="


    while pkill -u $UID -n -f $RE_PCONNECTOME
    do
        sleep 1
    done

    while pkill -u $UID -n -f $RE_PSTAT
    do
        sleep 1
    done
}

nrun=$1
NRCERO=$(printf "%03d" $nrun)
ODOM_RESULT_FILE="odom_result.$NRCERO.csv"


[ -a "$ODOM_RESULT_FILE" ] \
    && mv "$ODOM_RESULT_FILE" "$ODOM_RESULT_FILE.old"

# rm $STAT_RANGE_RESULT_FILE

. install/setup.bash

# https://answers.gazebosim.org//question/21103/exception-sending-a-message/
export IGN_IP=127.0.0.1

kill_zombies

sleep 1

ros2 launch food_experiment gradient.launch.py \
        "nrun:=$nrun" "odom_result_file:=$ODOM_RESULT_FILE" # "max_time:=1800"

RES=$?

sleep 1

kill_zombies

exit $RES

