#!/bin/bash

DIRNAME="$(dirname "$0")"

DISTS_COLLISION_DEF="$(seq 0 $COLLISION_STEP 1000)"  # 0 100 ... 1000

set -x

NPROC=${NPROC:-10}

NRUNS_START=${NRUNS_START:-0} # Default 0
NRUNS=${NRUNS:-100}

COLLISION_STEP=${COLLISION_STEP:-100}

DISTS_COLLISION=${DISTS_COLLISION:-$DISTS_COLLISION_DEF}

LINEAR_SPEEDF=${LINEAR_SPEEDF:-1.0}

WORLD=${WORLD:-corridor.world}

RANDOM_INIT=${RANDOM_INIT:-no}

{ set +x; } 2>/dev/null # Temporary remove debug trace

echo -n "Press Enter to continue"
read

# Have to create one net for each to isolate ros2
for i in $( seq 0  $(($NPROC-1)))
do
    docker network rm worm$i
    docker network create worm$i
done
set -x

for dc in $DISTS_COLLISION
do
    for batch in $(seq $NRUNS_START $NPROC $(($NRUNS_START+$NRUNS-1)))
    do
        echo Begin batch $batch
        NAMES=""
        for i in $(seq 0 $(($NPROC-1)))
        do
            nrun=$(($batch+$i))
            if [ $nrun -lt $(($NRUNS_START+$NRUNS)) ]
            then
                NAME=$("$DIRNAME/../docker/ros-worm-matrix/run.sh" \
                        --network $i worm-matrix/docker/bin/stat_collision1.sh $dc $nrun \
                        $LINEAR_SPEEDF world:=$WORLD random_init:=$RANDOM_INIT)
                [ $? == 0 ] || break 2
                NAMES="$NAMES $NAME"
            fi
        done
        docker wait $NAMES
    done
done

for i in $( seq 0  $(($NPROC-1)))
do 
    docker network rm worm$i
done
