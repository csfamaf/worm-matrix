#!/bin/bash

# set -x

kill_zombies() {

    while pkill -u $UID gzclient
    do
        sleep 1
    done

    while pkill -u $UID gzserver
    do
        sleep 1
    done

    RE_PCONNECTOME="python.*worm_connectome/connectome.*__node:="
    RE_PSTAT="python.*worm_connectome/stat_range.*$STAT_RANGE_RESULT_FILE.*__node:="


    while pkill -u $UID -n -f $RE_PCONNECTOME
    do
        sleep 1
    done

    while pkill -u $UID -n -f $RE_PSTAT
    do
        sleep 1
    done
}

nrun=$1
NRCERO=$(printf "%03d" $nrun)
STAT_RANGE_RESULT_FILE="stat_range_result.$NRCERO.csv"
ODOM_RESULT_FILE="odom_result.$NRCERO.csv"
CELLS_OUT_FILE="cells.$NRCERO.csv"
CELLS_INIT=$2


[ -a "$STAT_RANGE_RESULT_FILE" ] \
    && mv "$STAT_RANGE_RESULT_FILE" "$STAT_RANGE_RESULT_FILE.old"

[ -a "$ODOM_RESULT_FILE" ] \
    && mv "$ODOM_RESULT_FILE" "$ODOM_RESULT_FILE.old"

# rm $STAT_RANGE_RESULT_FILE

. install/setup.bash

# https://answers.gazebosim.org//question/21103/exception-sending-a-message/
export IGN_IP=127.0.0.1

kill_zombies

sleep 1

if [ -z "$CELLS_INIT" ]
then
    ros2 launch worm_connectome stat_range.launch.py \
        "nrun:=$nrun" "stat_range_result_file:=$STAT_RANGE_RESULT_FILE" \
        "odom_result_file:=$ODOM_RESULT_FILE" \
        "cells_out_file:=$CELLS_OUT_FILE"
else
    ros2 launch worm_connectome stat_range.launch.py \
        "nrun:=$nrun" "stat_range_result_file:=$STAT_RANGE_RESULT_FILE" \
        "odom_result_file:=$ODOM_RESULT_FILE" \
        "cells_out_file:=$CELLS_OUT_FILE" \
        "cells_init:=$CELLS_INIT"
fi

RES=$?

sleep 1

kill_zombies

exit $RES

