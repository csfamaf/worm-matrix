#!/bin/bash

NRUNS=50

DIRNAME="$(dirname "$0")"

for nrun in $(seq 0 $[$NRUNS-1])
# for nrun in $(seq 15 29)
do
    "$DIRNAME/stat_food1.sh" $nrun $CELLS_INIT

    [ $? == 0 ] || break
done
