#!/bin/bash

ACTIVE_NEURONS=10
#ACTIVE_NEURONS=75

DIR_PERTURBS="files/perturb${ACTIVE_NEURONS}_from${ACTIVE_NEURONS}"

NRUNS=$(ls $DIR_PERTURBS/*small_perturb_${ACTIVE_NEURONS}_random_neurons.csv | wc -l)

echo $NRUNS

DIRNAME="$(dirname "$0")"

cd "$DIRNAME/.."

. install/setup.bash

#ros2 run worm_connectome disembodied path_perturb${ACTIVE_NEURONS}_from${ACTIVE_NEURONS}.csv \
#  cells_perturb${ACTIVE_NEURONS}_from${ACTIVE_NEURONS}.csv \
#  files/small_perturb_${ACTIVE_NEURONS}_random_neurons.csv


for nrun in $(seq 0 $[$NRUNS-1])
# for nrun in $(seq 15 29)
do
    CELLS_INIT=$(printf "%04d" $nrun)small_perturb_${ACTIVE_NEURONS}_random_neurons.csv

    NRUN0=$(printf "%04d" $nrun)
    ros2 run worm_connectome disembodied path.$NRUN0.csv cells.$NRUN0.csv $DIR_PERTURBS/$CELLS_INIT

    [ $? == 0 ] || break
done



