#!/bin/bash

NRUNS=1000

ACTIVE_NEURONS=10
#ACTIVE_NEURONS=75

DIRNAME="$(dirname "$0")"

cd "$DIRNAME/.."

. install/setup.bash

for nrun in $(seq 0 $[$NRUNS-1])
# for nrun in $(seq 15 29)
do
    CELLS_INIT=$(printf "%02d" $nrun)small_perturb_${ACTIVE_NEURONS}_random_neurons.csv

    python3 src/worm_connectome/worm_connectome/create_small_perturb_random_neurons.py \
      dist0 $ACTIVE_NEURONS > src/worm_connectome/files/$CELLS_INIT

    [ $? == 0 ] || break

    NRUN0=$(printf "%03d" $nrun)
    ros2 run worm_connectome disembodied path.$NRUN0.csv cells.$NRUN0.csv src/worm_connectome/files/$CELLS_INIT

    [ $? == 0 ] || break
done



