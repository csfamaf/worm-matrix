#!/bin/bash
set -x
FILE=$1

NL=$(wc -l < $FILE)

MED=$[$NL/2]

cut -d\  -f 4 $FILE | LC_ALL=C sort -n | head -n $MED

echo NL=$NL MED=$MED