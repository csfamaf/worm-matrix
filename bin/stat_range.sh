#!/bin/bash

NRUNS=15

# If $1 not set use 0 value neurons
if [ -n "$1" ]
then

    CELLS_INIT=$(printf "%02d" $1)small_perturb_random_neurons.csv

fi

DIRNAME="$(dirname "$0")"

#[ -a "$STAT_RANGE_RESULT_FILE" ] \
#    && mv "$STAT_RANGE_RESULT_FILE" "$STAT_RANGE_RESULT_FILE.old"

# rm $STAT_RANGE_RESULT_FILE

. install/setup.bash

# https://answers.gazebosim.org//question/21103/exception-sending-a-message/
export IGN_IP=127.0.0.1

for nrun in $(seq 0 $[$NRUNS-1])
# for nrun in $(seq 15 29)
do
    "$DIRNAME/stat_range1.sh" $nrun $CELLS_INIT

    [ $? == 0 ] || break
done
