#/bin/bash

. install/setup.bash

[ $? == 0 ] || exit

exec ros2 launch worm_connectome worm_connectome.launch.py world:=maze01.world x-robot:=0.15 "$@"
