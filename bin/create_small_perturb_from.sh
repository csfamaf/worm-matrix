#!/bin/bash

NRUNS=1000

ACTIVE_NEURONS=10
#ACTIVE_NEURONS=75

CELLS_ZERO="files/small_perturb_${ACTIVE_NEURONS}_random_neurons.csv"

DIRNAME="$(dirname "$0")"

cd "$DIRNAME/.."

. install/setup.bash

for nrun in $(seq 0 $[$NRUNS-1])
# for nrun in $(seq 15 29)
do

    CELLS_INIT=$(printf "%04d" $nrun)small_perturb_${ACTIVE_NEURONS}_random_neurons.csv

    echo Create $CELLS_INIT

    python3 src/worm_connectome/worm_connectome/create_small_perturb_random_neurons.py \
      dist0 $ACTIVE_NEURONS $CELLS_ZERO > $CELLS_INIT

    [ $? == 0 ] || break

done



