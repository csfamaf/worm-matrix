#!/bin/bash

set -x

colcon test --packages-select ${1:-worm_connectome}
colcon test-result --test-result-base build/${1:-worm_connectome}/ --verbose
