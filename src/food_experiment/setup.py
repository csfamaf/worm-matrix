from os import path
from setuptools import setup
from glob import glob

package_name = 'food_experiment'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (path.join('share', package_name), glob('launch/*.launch.py')),
        (path.join('share', package_name, 'worlds'), glob('../../worlds/*')),
        (path.join('share', package_name, 'files'), glob('files/*')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='damian',
    maintainer_email='damian@famaf.unc.edu.ar',
    description='Move robot with connectome. Also, with teleop for testing',
    license='Apache License 2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'gradient = food_experiment.run_gradient:main',
            'odom = food_experiment.odom:main',
        ],
    },
)
