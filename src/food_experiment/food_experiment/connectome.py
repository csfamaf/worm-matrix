# coding=utf8
# GoPiGo Connectome
# Written by Timothy Busbice, Gabriel Garrett, Geoffrey Churchill (c) 2014, in Python 2.7
# The GoPiGo Connectome uses a postSynaptic dictionary based on the C Elegans Connectome Model
# This application can be ran on the Raspberry Pi GoPiGo robot with a Sonar that represents Nose
# Touch when activated
# To run standalone without a GoPiGo robot, simply comment out the sections with Start and End
# comments

# TIME STATE EXPERIMENTAL OPTIMIZATION
# The previous version had a logic error whereby if more than one neuron fired into the same
# neuron in the next time state, it would overwrite the contribution from the previous neuron.
# Thus, only one neuron could fire into the same neuron at any given time state.
# This version also explicitly lists all left and right muscles, so that during the muscle checks
# for the motor control function, instead of iterating through each neuron, we now iterate only
# through the relevant muscle neurons.

from .synaptics import Synaptics

from .robot import Robot
from .controller import Controller

import csv
import numpy as np

# from typing import Dict, Tuple

# disembodied is now class constructor arg
# import argparse
#
# parser = argparse.ArgumentParser()
# parser.add_argument('-d', '--disembodied', help='Run without sensor data', action='store_true')
# parser.add_argument('-v', '--verbose', action='count', default=0)
# # three levels of verbosity: [], -v, -vv
# # 0: only print starting and exiting messages
# # 1: print only when obstacles/food found
# # 2: print speed, left and right every time
# disembodied = parser.parse_args().disembodied
# verbosity = parser.parse_args().verbose
#
# print("Running on Robot: " + str(not disembodied))
#
# if not disembodied:
#     from gopigo import fwd, bwd, left_rot, right_rot, stop, set_speed, us_dist, volt
#
#     from di_sensors.distance_sensor import DistanceSensor  # utilizo el sensor de proximidad
#
#     # print("Example program for reading a Dexter Industries Distance Sensor on an I2C port.")
#
#
# # establish communication with the DistanceSensor
#
#     ds = DistanceSensor()
#
#     # set the sensor in fast-polling-mode
#
#     ds.start_continuous()

###############################################################################
#                                                                             #
#                    Declaracion de Variables y Constantes                    #
#                                                                             #
###############################################################################

# pauso el programa 40 segundos
# print("pausa")
# time.sleep(40)
# Variables:


# Declaro las variables globales que me dan acceso a los distintos estados
# Estas variables serviran como indice de la lista que contiene los valores
# acomulados en cada instante de tiempo (Nota: en python el primer elemento
# de un arreglo es 0)


# The postSynaptic dictionary contains the accumulated weighted values as the
# connectome is executed


###############################################################################
#                                                                             #
#                       Vector acciones del robot                             #
#                                                                             #
###############################################################################

# creo un vector vacio donde almacenare  las acciones del robot en cada instante
# de tiempo

# se detalla las distintas acciones que puede realizar el roboto y su respectiva
# correspondecia numerica


Detenido = "Detenido"  # el robot esta en estado detenido

# acciones si  nariz fue estimulada
Marcha_atras_izquierda_nariz = "Marcha atras izquierda nariz"
Marcha_atras_nariz = "Marcha atras nariz"
Marcha_atras_derecha_nariz = "Marcha atras derecha nariz"
Marcha_adelante_izquierda_nariz = "Marcha adelante izquierda nariz"
Marcha_adelante_nariz = "Marcha adelante nariz"
Marcha_adelante_derecha_nariz = "Marcha adelante derecha nariz"
Rotar_derecha_nariz = "Rotar derecha nariz"
Rotar_izquierda_nariz = "Rotar izquierda nariz"
Detenido_nariz = "Detenido_nariz"

# acciones si nariz no fue estimulada

# mueve el robot hacia atras y gira la llanta izquierda
Marcha_atras_izquierda = "Marcha atras izquierda"

# el robot retrocede en linea recta
Marcha_atras = "Marcha atras"

# mueve el robot hacia atras y gira la llanta derecha
Marcha_atras_derecha = "Marcha atras derecha"

# mueve el robot hacia adelante y gira la llanta izquierda
Marcha_adelante_izquierda = "Marcha adelante izquierda"

# el robot avanza en linea recta
Marcha_adelante = "Marcha adelante"

# mueve el robot hacia adelante y gira la llanta derecha
Marcha_adelante_derecha = "Marcha adelante derecha"

Rotar_derecha = "Rotar derecha"  # el robot rota a la derecha
Rotar_izquierda = "Rotar izquierda"  # el robot rota a la izquierda


class Connectome(Synaptics, Controller):

    ady_matrix_default = "ady_matrix_timothy.json"

    def __init__(self, robot: Robot, logger_getter, verbosity=False,
                 file_init_connectome=None, file_out_cells=None,
                 ady_matrix_file=None, inh_scale=None):

        #############################################################################
        #############################################################################
        #                                                                           #
        #                         Programa principal main()                         #
        #                                                                           #
        #############################################################################
        #############################################################################
        if not ady_matrix_file:
            ady_matrix_file = self.ady_matrix_default

        Synaptics.__init__(self, ady_matrix_file, inh_scale=inh_scale)
        Controller.__init__(self, robot, logger_getter, verbosity=verbosity)

        # self.dist = 0

        # Accumulators are used to decide the value to send to the Left and Right motors
        # of the GoPiGo robot
        # Could be local to motorcontrol?
        self.accumleft = 0
        self.accumright = 0

        self.flag_nariz = False  # First put will initialize again

        self.robot.set_speed(120)

        # def main():
        # """Here is where you would put in a method to stimulate the neurons
        #     We stimulate chemosensory neurons constantly unless nose touch
        #     (sonar) is stimulated and then we fire nose touch neurites.
        #     """

        self.tfood = 0
        self.niter = 0

        # Load connectome
        self.file_init_connectome = file_init_connectome
        if self.file_init_connectome:
            self.__load_connectome()

        if file_out_cells:
            f = open(file_out_cells, 'w')
            self.__file_out_cells = f
            self.__out_cells = csv.writer(f)
            self.__out_cells.writerow(['sec', 'nanosec'] + self.cells)
            self.__save_cells()
            # self.__save_cells(time=(0, -10 ** 9 // 2))
        else:
            self.__out_cells = None

        self.get_logger().info("Use {} ady matrix file".format(ady_matrix_file))
        self.get_logger().info("Inhibitory scale = {}".format(inh_scale))
        self.get_logger().info("Verbosity = {}".format(verbosity))

    def __del__(self):
        if hasattr(self, "__out_cells") and self.__out_cells:
            self.__file_out_cells.close()

    def __load_connectome(self):

        with open(self.file_init_connectome, 'r', newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            neurons_init = next(reader, None)
            for neuron, value in neurons_init.items():
                self.postSynaptic[neuron][self.nextState] = int(value)
            if self.verbosity > 0:
                self.get_logger().info(
                    "Init {} neurons from {} file".format(len(neurons_init),
                                                          self.file_init_connectome))

    def __save_cells(self, time=None):
        if self.__out_cells:
            if time:
                sec, nanosec = time
            else:
                sec, nanosec = self.robot.get_time()
            cvals = [self.postSynaptic[c][self.nextState] for c in self.cells]
            self.__out_cells.writerow([sec, nanosec] + cvals)
            self.__file_out_cells.flush()

    def listener_callback(self, msg):

        self.get_logger().info("Iter: {}".format(self.niter))

        gradient_food = self.robot.ds_read(msg)  # leo la distancia en mm

        self.get_logger().info("Gradient food: {}".format(gradient_food))

        if np.random.random() < gradient_food:
            self.get_logger().info("Estimulate food")
            self.stimulate_food()

        self.runconnectome()
        self.__save_cells()

        # if 0 < self.dist < 300:
        #     if self.verbosity > 0:
        #         self.get_logger().info("OBSTACLE (Nose Touch) {}".format(self.dist))

        #     self.flag_nariz = True

        #     self.stimulate_nose()
        #     self.runconnectome()
        #     self.__save_cells()

        # else:  # not (0 < self.dist < 300)

        #     if self.tfood < 2:
        #         if self.verbosity > 0:
        #             self.get_logger().info("FOOD")

        #         self.flag_nariz = False

        #         self.stimulate_food()
        #         self.runconnectome()
        #         self.robot.time_sleep(0.5)
        #         self.__save_cells()

        #     # if not (0 < self.dist < 300) and tfood >= 2
        #     # ===> not runconnectome() and not motorcontrol()
        #     self.tfood += 0.5
        #     if self.tfood > 20:
        #         self.tfood = 0

        self.niter += 1

    ###############################################################################
    #                                                                             #
    #                      Procedimiento que corre el conectoma                   #
    #                                                                             #
    ###############################################################################

    # en este procedimiento se corre el conectoma del gusano cuando una cierta cantidad
    # de neuornas se estimula, si alguna neurona dispara se activara el control del
    # robot y volvera a ponerse a cero los pesos acomulados de esta.

    def runconnectome(self):
        """
        Update de connectome after sensor stimulus.

        Each time a set of neuron is stimulated, this method will execute
        The weigted values are accumulated in the postSynaptic array
        Once the accumulation is read, we see what neurons are greater
        then the threshold and fire the neuron or muscle that has exceeded
        the threshold.
        """
        self.fire_neurons()

        self.motorcontrol()

        self.swapStates()

    ###############################################################################
    #                                                                             #
    #              Procedimiento que hace mover el robot                          #
    #                                                                             #
    ###############################################################################

    def motorcontrol(self):

        for muscle in self.muscleBodyList:  # if this doesn't work, do muscle in postSynaptic
            if muscle in self.mBodyLeft:
                self.accumleft += self.postSynaptic[muscle][self.nextState]  # vs thisState???

                # print muscle, "Before", postSynaptic[muscle][thisState], accumleft
                self.postSynaptic[muscle][self.nextState] = 0
                # print muscle, "After", postSynaptic[muscle][thisState], accumleft

            elif muscle in self.mBodyRight:
                self.accumright += self.postSynaptic[muscle][self.nextState]  # vs thisState???

                # postSynaptic[muscle][self.thisState] = 0
                self.postSynaptic[muscle][self.nextState] = 0

        # We turn the wheels according to the motor weight accumulation
        if self.accumleft != 0:
            self.get_logger().info("accumleft: {}, accumright: {}, turnratio: {}".format(
                self.accumleft, self.accumright, float(self.accumright) / float(self.accumleft)))
        else:
            self.get_logger().info("accumleft: {}, accumright: {}".format(
                self.accumleft, self.accumright))

        new_speed = abs(self.accumleft) + abs(self.accumright)
        if new_speed > 150:
            new_speed = 150
        elif new_speed < 75:
            new_speed = 75

        self.robot.set_speed(new_speed)

        # dependiendo de los valores de accumleft y  accumright, el robot hara
        # las siguientes acciones:

        # * Detenido
        # * Mueve el robo hacia atras, existe dos opciones:
        #   * rota a la izquierda
        #   * rota a la derecha
        # * Solo rota a la derecha
        # * Solo rota a la izquierda
        # * Mueve el robo hacia adelante, existe dos opciones:
        #   * rota a la izquierda
        #   * rota a la derecha

        if self.accumleft == 0 and self.accumright == 0:

            if self.flag_nariz:
                accion = Detenido_nariz
            else:
                accion = Detenido

            self.robot.stop()
        elif self.accumright <= 0 and self.accumleft < 0:
            self.robot.set_speed(150)
            turnratio = float(self.accumright) / float(self.accumleft)
            if turnratio <= 0.6:

                if self.flag_nariz:
                    accion = Marcha_atras_izquierda_nariz
                else:
                    accion = Marcha_atras_izquierda

                # En este estado el robot rota a la izquierda

                self.robot.left_rot()

                self.robot.time_sleep(0.8)

            elif turnratio >= 2:

                if self.flag_nariz:
                    accion = Marcha_atras_derecha_nariz
                else:
                    accion = Marcha_atras_derecha

                self.robot.right_rot()
                self.robot.time_sleep(0.8)

            else:

                if self.flag_nariz:
                    accion = Marcha_atras_nariz
                else:
                    accion = Marcha_atras

            self.robot.bwd()
            self.robot.time_sleep(0.5)

        elif self.accumright <= 0 <= self.accumleft:

            # print("rotar derecha")
            # En este estado el robot rota a la derecha
            if self.flag_nariz:
                accion = Rotar_derecha_nariz
            else:
                accion = Rotar_derecha

            self.robot.right_rot()
            self.robot.time_sleep(.8)

        elif self.accumright >= 0 >= self.accumleft:

            if self.flag_nariz:
                accion = Rotar_izquierda_nariz
            else:
                accion = Rotar_izquierda

            self.robot.left_rot()
            self.robot.time_sleep(.8)

        elif self.accumright >= 0 and self.accumleft > 0:
            turnratio = float(self.accumright) / float(self.accumleft)
            # print "Turn Ratio: ", turnratio
            if turnratio <= 0.6:

                if self.flag_nariz:
                    accion = Marcha_adelante_izquierda_nariz
                else:
                    accion = Marcha_adelante_izquierda

                self.robot.left_rot()
                self.robot.time_sleep(0.8)
            elif turnratio >= 2:

                if self.flag_nariz:
                    accion = Marcha_adelante_derecha_nariz
                else:
                    accion = Marcha_adelante_derecha

                self.robot.right_rot()
                self.robot.time_sleep(0.8)
            else:

                if self.flag_nariz:
                    accion = Marcha_adelante_nariz
                else:
                    accion = Marcha_adelante

            self.robot.fwd()
            self.robot.time_sleep(0.5)

        else:  # Never riched

            # print("detenido")
            accion = Detenido
            self.robot.stop()

        if self.verbosity:
            self.get_logger().info("Speed: {}, Action: {}".format(new_speed, accion))

        self.accumleft = 0
        self.accumright = 0
        # time.sleep(0.5)
