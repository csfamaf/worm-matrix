import rclpy
from rclpy.executors import SingleThreadedExecutor
from ament_index_python.packages import get_package_share_directory
import threading
import argparse
from os import path
import numpy as np

from .sensor_controller import SensorController
from .connectome import Connectome
from .sleeper import NodeSleeper


def main(args=None):

    rclpy.init(args=args)

    parser = argparse.ArgumentParser()

    parser.add_argument("-co", "--cells-out", help="File name output cells value through time")
    parser.add_argument("-ci", "--cells-init", help="File name with connectome initialization")
    parser.add_argument("-m", "--ady-matrix",
                        help="File name with connectome connectome ady matrix. Default Timothy")
    parser.add_argument("-is", "--inh-scale", type=float,
                        help="Scale for inhibitory connection. Default 1.0")
    parser.add_argument("-Q", "--quiet", action="store_true", help="Quiet. Default verbose")
    parser.add_argument("--random-seed", type=int, help="Random seed")
    parser.add_argument("--ros-args", action="store_true", help="ROS argument")
    parser.add_argument("--params-file", help="ROS argument")
    parser.add_argument("-r", help="ROS argument")

    args = parser.parse_args()

    if args.random_seed or args.random_seed == 0:
        np.random.seed(args.random_seed)
        print("random seed:", args.random_seed)
    else:
        print("random seed default")

    executor = SingleThreadedExecutor()

    sleeper = NodeSleeper(verbosity=True)

    executor.add_node(sleeper)
    thread = threading.Thread(target=executor.spin, args=(), daemon=True)
    thread.start()

    sleeper.wait_ready()

    if args.cells_init:
        file_cells_init_name = path.join(
            get_package_share_directory("worm_connectome"), "files", args.cells_init
        )
    else:
        file_cells_init_name = None

    print("time sleeper: ", sleeper.get_time())

    connectome_worm = SensorController(Connectome, sleeper, verbosity=not args.quiet,
                                       file_out_cells=args.cells_out,
                                       file_init_connectome=file_cells_init_name,
                                       ady_matrix_file=args.ady_matrix,
                                       inh_scale=args.inh_scale)

    print('To finish the controller do:',
          'ros2 service call /worm/change_state lifecycle_msgs/srv/ChangeState'
          ' "{transition: {id: 8}}"')

    try:
        # Run until Future by receiving "Destroy" with service
        rclpy.spin_until_future_complete(connectome_worm, connectome_worm.done)
    except KeyboardInterrupt:
        # Destroy the node explicitly
        # (optional - otherwise it will be done automatically
        # when the garbage collector destroys the node object)
        # connectome_worm.destroy_node()
        # rclpy.shutdown()
        print("Interrupted")
        # sys.exit()

    sleeper.destroy_node()
    connectome_worm.destroy_node()
    rclpy.shutdown()
    print("Done")


if __name__ == '__main__':
    main()
