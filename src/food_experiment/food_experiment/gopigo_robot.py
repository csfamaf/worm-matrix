from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry

from .sleeper import Sleeper
from .robot import Robot


class GoPiGoRobot(Robot):

    gradient_begin = 30.0  # meters
    gradient_begin2 = gradient_begin ** 2

    def __init__(self, publisher, sleeper: Sleeper, logger_getter, verbosity=0, speed=75):

        super().__init__(sleeper)

        self.verbosity = verbosity
        self.motor = publisher
        self.sleeper = sleeper
        self.get_logger = logger_getter
        self._speed = 0.0

        self._tw = Twist()
        self.zero_twist()

        self.set_speed(speed)

    def zero_twist(self):
        self._tw.linear.y = 0.0
        self._tw.linear.z = 0.0
        self._tw.angular.y = 0.0
        self._tw.angular.x = 0.0

        self._tw.linear.x = 0.0  # meter per second
        self._tw.angular.z = 0.0  # radians per second

    def send_command(self):
        self.motor.publish(self._tw)
        if self.verbosity:
            self.get_logger().info(
                "Twist (x,z) = ({:.2f},{:.2f})".format(self._tw.linear.x, self._tw.angular.z)
            )

    def set_speed(self, speed):
        self._speed = self.speed_factor * abs(speed)
        if self.verbosity > 2:
            self.get_logger().info(
                "Speed (Controller, Simul) = ({},{:.2f})".format(speed, self._speed)
            )

    def fwd(self):
        self.zero_twist()
        self._tw.linear.x = self._speed
        self.send_command()

    def bwd(self):
        self.zero_twist()
        self._tw.linear.x = - self._speed
        self.send_command()

    def left_rot(self):
        self.zero_twist()
        self._tw.angular.z = self.angular_factor * self._speed
        self.send_command()

    def right_rot(self):
        self.zero_twist()
        self._tw.angular.z = -self.angular_factor * self._speed
        self.send_command()

    def stop(self):
        self.zero_twist()
        self.send_command()

    # def _gradient_food(self, x: float, y: float):
    #     """Ramp Gradient."""
    #     if y > self.gradient_begin:
    #         return 1.0
    #     elif y < -self.gradient_begin:
    #         return 0.1
    #     else:
    #         return (9.0*y + 11.0*self.gradient_begin) / (20.0*self.gradient_begin)

    # def _gradient_food(self, x: float, y: float):
    #     """Constant Gradient."""
    #     return 0.5

    def _gradient_food(self, x: float, y: float):
        """Cone Gradient."""
        dist2 = x ** 2 + y ** 2

        if dist2 > self.gradient_begin2:
            return 1.0
        else:
            return 0.9*dist2/self.gradient_begin2 + 0.1

    def ds_read(self, msg: Odometry):
        """Receive odometer data. Return food gradient according x position."""
        gradient_food = self._gradient_food(msg.pose.pose.position.x, msg.pose.pose.position.y)

        if self.verbosity > 1:
            self.get_logger().info("Gradient: {:.2f}".format(gradient_food))

        return gradient_food
