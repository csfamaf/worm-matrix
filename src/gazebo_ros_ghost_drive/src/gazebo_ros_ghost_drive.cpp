// Copyright (c) 2010, Daniel Hewlett, Antons Rebguns
// All rights reserved.
//
// Software License Agreement (BSD License 2.0)
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
//  * Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//  * Neither the name of the company nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
// ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

/*
 * \file  gazebo_ros_diff_drive.cpp
 *
 * \brief A differential drive plugin for gazebo. Based on the diffdrive plugin
 * developed for the erratic robot (see copyright notice above). The original
 * plugin can be found in the ROS package gazebo_erratic_plugins.
 *
 * \author  Piyush Khandelwal (piyushk@gmail.com)
 *
 * $ Id: 06/21/2013 11:23:40 AM piyushk $
 */

/*
 *
 * The support of acceleration limit was added by
 * \author   George Todoran <todorangrg@gmail.com>
 * \author   Markus Bader <markus.bader@tuwien.ac.at>
 * \date 22th of May 2014
 */

#include <gazebo/common/Time.hh>
#include <gazebo/physics/Joint.hh>
#include <gazebo/physics/Link.hh>
#include <gazebo/physics/Model.hh>
#include <gazebo/physics/World.hh>
#include <gazebo_ros/conversions/geometry_msgs.hpp>
#include "gazebo_ros_ghost_drive/gazebo_ros_ghost_drive.hpp"
#include <gazebo_ros/node.hpp>
#include <geometry_msgs/msg/pose2_d.hpp>
#include <geometry_msgs/msg/twist.hpp>
#include <sdf/sdf.hh>

#ifdef NO_ERROR
// NO_ERROR is a macro defined in Windows that's used as an enum in tf2
#undef NO_ERROR
#endif

#include <tf2_ros/transform_broadcaster.h>

#include <memory>
#include <string>
#include <vector>

namespace gazebo_plugins {
    class GazeboRosGhostDrivePrivate {
    public:

        /// Callback to be called at every simulation iteration.
        /// \param[in] _info Updated simulation info.
        void OnUpdate(const gazebo::common::UpdateInfo &_info);

        /// Callback when a velocity command is received.
        /// \param[in] _msg Twist command message.
        void OnCmdVel(const geometry_msgs::msg::Twist::SharedPtr _msg);

        gazebo::physics::LinkPtr link_to_move;

        std::basic_string<char> link_to_move_name;

        /// A pointer to the GazeboROS node.
        gazebo_ros::Node::SharedPtr ros_node_;

        /// Subscriber to command velocities
        rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr cmd_vel_sub_;

        /// Connection to event called at every world iteration.
        gazebo::event::ConnectionPtr update_connection_;

        /// Pointer to model.
        gazebo::physics::ModelPtr model_;

        /// Protect variables accessed on callbacks.
        std::mutex lock_;

        /// Linear velocity in X received on command (m/s).
        double target_x_{0.0};

        /// Angular velocity in Z received on command (rad/s).
        double target_rot_{0.0};

        /// Update period in seconds.
        double update_period_{0.0};

        /// Last update time.
        gazebo::common::Time last_update_time_;

        /// Keep encoder data.
        geometry_msgs::msg::Pose2D pose_encoder_;

    };

    GazeboRosGhostDrive::GazeboRosGhostDrive()
            : impl_(std::make_unique<GazeboRosGhostDrivePrivate>()) {
    }

    GazeboRosGhostDrive::~GazeboRosGhostDrive() = default;

    void GazeboRosGhostDrive::Load(gazebo::physics::ModelPtr _model, sdf::ElementPtr _sdf) {
        impl_->model_ = _model;

        // Initialize ROS node
        impl_->ros_node_ = gazebo_ros::Node::Get(_sdf);

        impl_->link_to_move_name = _sdf->GetElement("link_to_move")->Get<std::string>();
        impl_->link_to_move = _model->GetLink(impl_->link_to_move_name);

        if (!impl_->link_to_move) {
            RCLCPP_ERROR(impl_->ros_node_->get_logger(),
                         "Link [%s] not found, plugin will not work.", impl_->link_to_move_name.c_str());
            impl_->ros_node_.reset();
            return;
        }

        // Update rate
        auto update_rate = _sdf->Get<double>("update_rate", 100.0).first;
        if (update_rate > 0.0) {
            impl_->update_period_ = 1.0 / update_rate;
        } else {
            impl_->update_period_ = 0.0;
        }
        impl_->last_update_time_ = _model->GetWorld()->SimTime();

        impl_->cmd_vel_sub_ = impl_->ros_node_->create_subscription<geometry_msgs::msg::Twist>(
                "cmd_vel", rclcpp::QoS(rclcpp::KeepLast(1)),
                std::bind(&GazeboRosGhostDrivePrivate::OnCmdVel, impl_.get(), std::placeholders::_1));

        RCLCPP_INFO(impl_->ros_node_->get_logger(), "Subscribed to [%s]",
                    impl_->cmd_vel_sub_->get_topic_name());

        // Listen to the update event (broadcast every simulation iteration)
        impl_->update_connection_ = gazebo::event::Events::ConnectWorldUpdateBegin(
                std::bind(&GazeboRosGhostDrivePrivate::OnUpdate, impl_.get(), std::placeholders::_1));
    }

    void GazeboRosGhostDrive::Reset() {
        impl_->last_update_time_ =
                impl_->link_to_move->GetWorld()->SimTime();

        impl_->pose_encoder_.x = 0;
        impl_->pose_encoder_.y = 0;
        impl_->pose_encoder_.theta = 0;
        impl_->target_x_ = 0;
        impl_->target_rot_ = 0;
    }

    void GazeboRosGhostDrivePrivate::OnUpdate(const gazebo::common::UpdateInfo &_info) {

        double seconds_since_last_update = (_info.simTime - last_update_time_).Double();

        if (seconds_since_last_update < update_period_) {
            return;
        }

        double vr = target_x_;
        double va = target_rot_;

        auto pose = model_->WorldPose();

        geometry_msgs::msg::Point position;
        position = gazebo_ros::Convert<geometry_msgs::msg::Point>(pose.Pos());

//        geometry_msgs::msg::Quaternion orientation;
//        orientation = gazebo_ros::Convert<geometry_msgs::msg::Quaternion>(pose.Rot());
        const double &orientation = pose.Rot().Euler().Z();

//        RCLCPP_INFO(ros_node_->get_logger(), "Pos (x,y,z): (%f, %f, %f), Orientation: %f",
//                    position.x, position.y, position.z, orientation);

        double vx = vr * cos(orientation);
        double vy = vr * sin(orientation);

        link_to_move->SetLinearVel({vx, vy, 0});
        link_to_move->SetAngularVel({0, 0, va});

        last_update_time_ = _info.simTime;
    }

    void GazeboRosGhostDrivePrivate::OnCmdVel(const geometry_msgs::msg::Twist::SharedPtr _msg) {
        std::lock_guard<std::mutex> scoped_lock(lock_);
        target_x_ = _msg->linear.x;
        target_rot_ = _msg->angular.z;
    }


    GZ_REGISTER_MODEL_PLUGIN(GazeboRosGhostDrive)
}  // namespace gazebo_plugins
