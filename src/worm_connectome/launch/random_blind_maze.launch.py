from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import ExecuteProcess
from launch.actions import Shutdown
from launch.substitutions import LaunchConfiguration
from launch import LaunchContext
import os
from ament_index_python.packages import get_package_share_directory
import sys


def shutdown_if_not_shutdown(_, launch_context: LaunchContext):
    if not launch_context.is_shutdown:
        return Shutdown()


# Get sensor controller from commad line
# Default connectome
def get_sensor_controller_name():

    param = [p for p in sys.argv if p.startswith("controller:=")]

    return param[0].split(":=")[1] if param else "connectome"


def generate_launch_description():

    world_file_name = 'maze01.world'
    physic = "x15"  # xN time faster (defined in .world file)
    robot_file_name = 'gopigo02'
    sensor_controller = get_sensor_controller_name()

    # Rate of clock messages
    # (https://github.com/ros-simulation/gazebo_ros_pkgs/wiki/
    # ROS-2-Migration:-ROS-Clocks-and-sim-time#clock-rate)
    world_params_file_name = "empty_time_rate.params.yaml"

    world = os.path.join(get_package_share_directory('worm_connectome'), 'worlds', world_file_name)

    world_params = os.path.join(get_package_share_directory('worm_connectome'), 'worlds',
                                world_params_file_name)

    nrun = LaunchConfiguration('nrun')
    odom_result_file = LaunchConfiguration('odom_result_file')

    return LaunchDescription([
        Node(
            package='worm_connectome',
            node_executable=sensor_controller,
            node_name='worm',
            parameters=[
                {"use_sim_time": True}
            ],
            output='full',
            emulate_tty=True,
        ),
        Node(
            package='robot_spawner_pkg',
            node_executable='spawn_worm',
            arguments=[robot_file_name, 'worm', '0.15', '0.0', '0.1'],
            node_name='spawner',
            parameters=[
                {"use_sim_time": True}
            ],
            output='screen',
            emulate_tty=True,
        ),
        Node(
            package='worm_connectome',
            node_executable='stat_maze',
            arguments=[nrun, odom_result_file],
            node_name='stat_maze',
            parameters=[
                {"use_sim_time": True}
            ],
            output='full',
            emulate_tty=True,
            on_exit=shutdown_if_not_shutdown
        ),
        ExecuteProcess(
            cmd=['gzserver', '--verbose', world, '-o',  physic,
                 '-s', 'libgazebo_ros_init.so', "--ros-args", "--params-file", world_params,
                 '-s',  'libgazebo_ros_factory.so'],
            output='own_log',
            sigterm_timeout='10',
            additional_env={"GAZEBO_PLUGIN_PATH": "build/my_diff_drive/"}
            # on_exit=
            # emulate_tty=False,
        ),
    ])
