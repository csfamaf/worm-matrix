from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import ExecuteProcess
from launch.actions import Shutdown
from launch.substitutions import LaunchConfiguration
from launch import LaunchContext
import os
from ament_index_python.packages import get_package_share_directory


def shutdown_if_not_shutdown(_, launch_context: LaunchContext):
    if not launch_context.is_shutdown:
        return Shutdown()


def generate_launch_description():

    default_world = 'corridor.world'
    physic = "x8"  # xN time faster (defined in .world file)
    # physic = "x10"  # xN time faster (defined in .world file)

    # Rate of clock messages
    # (https://github.com/ros-simulation/gazebo_ros_pkgs/wiki/
    # ROS-2-Migration:-ROS-Clocks-and-sim-time#clock-rate)
    worlds_path = os.path.join(get_package_share_directory('worm_connectome'), 'worlds')
    world = LaunchConfiguration('world', default=default_world)

    world_params_file_name = "empty_time_rate.params.yaml"
    world_params = os.path.join(worlds_path, world_params_file_name)

    robot_file_name = 'gopigo03'
    sensor_controller = 'connectome'

    nrun = LaunchConfiguration('nrun')
    # max_time = LaunchConfiguration('max_time')
    collision_result_file = LaunchConfiguration('collision_result_file')
    max_dist_collision = LaunchConfiguration('max_dist_collision')
    linear_speed_factor = LaunchConfiguration('linear_speed_factor', default="1.0")
    random_init = LaunchConfiguration('random_init', default="no")

    return LaunchDescription([
        Node(
            package='worm_connectome',
            executable=sensor_controller,
            name='worm',
            arguments=[
                '--max-dist-collision', max_dist_collision,
                '--linear-speed-factor', linear_speed_factor,
                '--random-init', random_init,
                '--seed', nrun,  # Seed es always nrun
            ],
            parameters=[
                {"use_sim_time": True}
            ],
            output='full',
            emulate_tty=True,
        ),
        Node(
            package='robot_spawner_pkg',
            executable='spawn_worm',
            arguments=[robot_file_name, 'worm', '0.0', '0.0', '0.1'],
            name='spawner',
            parameters=[
                {"use_sim_time": True}
            ],
            output='screen',
            emulate_tty=True,
        ),
        Node(
            package='worm_connectome',
            executable='stat_collision',
            arguments=[nrun, collision_result_file],
            name='stat_collision',
            parameters=[
                {"use_sim_time": True}
            ],
            output='full',
            emulate_tty=True,
            on_exit=shutdown_if_not_shutdown
        ),
        ExecuteProcess(
            cmd=['gzserver', '--verbose', [worlds_path, '/', world], '-o',  physic,
                 '-s', 'libgazebo_ros_init.so',
                 '-s',  'libgazebo_ros_factory.so',
                 "--ros-args", "--params-file", world_params],
            output='own_log',
            sigterm_timeout='10',
            additional_env={"GAZEBO_PLUGIN_PATH": "build/my_diff_drive/"}
            # on_exit=
            # emulate_tty=False,
        ),
    ])
