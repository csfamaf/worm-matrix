from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import ExecuteProcess
from launch.actions import Shutdown
from launch.substitutions import LaunchConfiguration
from launch import LaunchContext
import os
from ament_index_python.packages import get_package_share_directory


def shutdown_if_not_shutdown(_, launch_context: LaunchContext):
    if not launch_context.is_shutdown:
        return Shutdown()


def generate_launch_description():

    default_world = 'corridor.world'
    physic = "default_physics"  # xN time faster (defined in .world file)
    # physic = "x10"  # xN time faster (defined in .world file)
    worlds_path = os.path.join(get_package_share_directory('worm_connectome'), 'worlds')
    world = LaunchConfiguration('world', default=default_world)

    robot_file_name = 'gopigo03'
    sensor_controller = 'connectome'

    return LaunchDescription([
        Node(
            package='worm_connectome',
            executable=sensor_controller,
            name='worm',
            parameters=[
                {"use_sim_time": True}
            ],
            output='full',
            emulate_tty=True,
        ),
        Node(
            package='robot_spawner_pkg',
            executable='spawn_worm',
            arguments=[robot_file_name, 'worm', '0.0', '0.0', '0.1'],
            name='spawner',
            parameters=[
                {"use_sim_time": True}
            ],
            output='screen',
            emulate_tty=True,
        ),
        ExecuteProcess(
            cmd=['gzserver', '--verbose', [worlds_path, '/', world], '-o',  physic,
                 '-s', 'libgazebo_ros_init.so',  # "--ros-args",
                 '-s',  'libgazebo_ros_factory.so'],
            output='own_log',
            sigterm_timeout='10',
            additional_env={"GAZEBO_PLUGIN_PATH": "build/my_diff_drive/"}
            # on_exit=
            # emulate_tty=False,
        ),
    ])
