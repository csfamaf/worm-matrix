import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import ExecuteProcess
from launch.actions import RegisterEventHandler
from launch.actions import Shutdown
from launch.actions import DeclareLaunchArgument
from launch.actions import OpaqueFunction
from launch.event_handlers import OnProcessExit
from launch import LaunchContext
from launch.substitutions import LaunchConfiguration

# Parameters of launcher:
# world and gzclient for gazebo
# ady-matrix and inh-scale for run_connectome

robot_file_name = 'gopigo02'
# robot_file_name = 'two_wheel_scar_simple'
# robot_file_name = 'two_wheel_small_car'
ady_matrix_default = 'ady_matrix_timothy.json'
world_default = 'worm_playground.world'
gzclient_default = 'False'
inh_scale_default = '1'


def shutdown_if_not_shutdown(_, launch_context: LaunchContext):
    if not launch_context.is_shutdown:
        return Shutdown()


# Function to be opaque in generate_launch_description()
# in order to read parameter world:=... inside that function
def launch_setup_gzserver(context, *args, **kwargs):

    world_file_name = os.path.basename(LaunchConfiguration('world').perform(context))

    # Add resource file directory path
    world = os.path.join(get_package_share_directory('worm_connectome'), 'worlds', world_file_name)

    launch_gzclient = LaunchConfiguration('gzclient').perform(context)

    if launch_gzclient == 'False':
        print("Launch gserver only")
        gzserver_proc = ExecuteProcess(
            cmd=['gzserver', '--verbose', '-s', 'libgazebo_ros_factory.so', world],
            output='own_log',
            sigterm_timeout='10',
            additional_env={"GAZEBO_PLUGIN_PATH": "build/my_diff_drive/"}
            # on_exit=
            # emulate_tty=False,
        )
    else:
        print("Launch full gazebo")
        gzserver_proc = ExecuteProcess(
            cmd=['gazebo', '--verbose', '-s', 'libgazebo_ros_factory.so', world],
            output='own_log',
            sigterm_timeout='10',
            additional_env={"GAZEBO_PLUGIN_PATH": "build/my_diff_drive/"}
            # on_exit=
            # emulate_tty=False,
        )

    return [gzserver_proc]


def generate_launch_description():

    ady_matrix = LaunchConfiguration('ady-matrix')
    inh_scale = LaunchConfiguration('inh-scale')
    x_robot = LaunchConfiguration('x-robot')

    # It's referenced two times
    worm_connectome_node = Node(
        package='worm_connectome',
        node_executable='connectome',
        node_name='worm',
        arguments=["-m", ady_matrix, "--inh-scale", inh_scale],
        output='screen',
        emulate_tty=True,
    )

    return LaunchDescription([
        DeclareLaunchArgument('ady-matrix', default_value=ady_matrix_default),
        DeclareLaunchArgument('inh-scale', default_value=inh_scale_default),
        worm_connectome_node,
        DeclareLaunchArgument('x-robot', default_value='0.0'),
        Node(
            package='robot_spawner_pkg',
            node_executable='spawn_worm',
            arguments=[robot_file_name, 'worm', x_robot, '0.0', '0.1'],
            node_name='spawner',
            output='screen',
            emulate_tty=True,
        ),
        DeclareLaunchArgument('world', default_value=world_default),
        DeclareLaunchArgument('gzclient', default_value=gzclient_default),
        OpaqueFunction(function=launch_setup_gzserver),
        RegisterEventHandler(
            OnProcessExit(target_action=worm_connectome_node, on_exit=shutdown_if_not_shutdown)
        ),
        # RegisterEventHandler(
        #     OnProcessIO(on_stdout=lambda info: print(info.text))
        # )
    ])
