import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import ExecuteProcess
from launch.actions import RegisterEventHandler
from launch.actions import Shutdown
from launch.event_handlers import OnProcessExit
from launch import LaunchContext


def shutdown_if_not_shutdown(_, launch_context: LaunchContext):
    if not launch_context.is_shutdown:
        return Shutdown()


def generate_launch_description():

    world_file_name = 'worm_playground.world'
    robot_file_name = 'two_wheel_small_car'

    world = os.path.join(get_package_share_directory('worm_connectome'), 'worlds', world_file_name)
    # model = os.path.join(get_package_share_directory('robot_spawner_pkg'), 'models',
    #                      robot_file_name, 'model.sdf')

    worm_connectome_node = Node(
        package='worm_connectome',
        node_executable='connectome',
        node_name='worm',
        output='screen',
        emulate_tty=True,
    )

    return LaunchDescription([
        worm_connectome_node,
        Node(
            package='robot_spawner_pkg',
            node_executable='spawn_worm',
            arguments=[robot_file_name, 'worm', '0.0', '0.0', '0.1'],
            node_name='spawner',
            output='screen',
            emulate_tty=True,
        ),
        ExecuteProcess(
            cmd=['gzserver', '--verbose', '-s',  'libgazebo_ros_factory.so', world],
            output='own_log',
            sigterm_timeout='10',
            # on_exit=
            # emulate_tty=False,
        ),
        RegisterEventHandler(
            OnProcessExit(target_action=worm_connectome_node, on_exit=shutdown_if_not_shutdown)
        ),
        # RegisterEventHandler(
        #     OnProcessIO(on_stdout=lambda info: print(info.text))
        # )
    ])
