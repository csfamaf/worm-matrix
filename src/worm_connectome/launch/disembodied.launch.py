# Csv with diff_driver movements is in ~/.ros/log/<date>/ros2-2-stdout.log
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import ExecuteProcess
from launch.actions import Shutdown
from launch.actions import TimerAction
from launch import LaunchContext


def shutdown_if_not_shutdown(_, launch_context: LaunchContext):
    if not launch_context.is_shutdown:
        return Shutdown()


def generate_launch_description():

    return LaunchDescription([
        Node(
            package='worm_connectome',
            node_executable='connectome',
            node_name='worm',
            output='log',
            emulate_tty=True,
        ),
        ExecuteProcess(
            cmd=["ros2", "topic", "echo", "--csv", "/worm/cmd_worm"],
            output='own_log',
            output_format='{line}',
            emulate_tty=True,
        ),
        TimerAction(
            period=3.0,
            actions=[
                ExecuteProcess(
                    cmd=["ros2", "topic", "pub", "-r", "100", "-p", "100",
                         "worm/laser_scan", "sensor_msgs/msg/Range", "{range: 30}"],
                    output='own_log',
                ),
            ]
        ),
    ])
