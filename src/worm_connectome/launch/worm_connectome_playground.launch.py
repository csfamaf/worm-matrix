import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import ExecuteProcess
# from launch.actions import RegisterEventHandler
# from launch.event_handlers import OnProcessIO


def generate_launch_description():

    world_file_name = 'worm_playground_with_car.world'
    world = os.path.join(get_package_share_directory('worm_connectome'), 'worlds', world_file_name)

    return LaunchDescription([
        Node(
            package='worm_connectome',
            node_executable='connectome',
            node_name='sim',
            output='screen',
            emulate_tty=True,
        ),
        ExecuteProcess(
            cmd=['gzserver', '--verbose', world],
            output='own_log',
            sigterm_timeout='10',
            # on_exit=
            # emulate_tty=False,
        ),
        # RegisterEventHandler(
        #     OnProcessIO(on_stdout=lambda info: print(info.text))
        # )
    ])
