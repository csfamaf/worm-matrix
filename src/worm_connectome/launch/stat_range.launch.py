from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import ExecuteProcess
from launch.actions import Shutdown
from launch.actions import TimerAction
from launch.substitutions import LaunchConfiguration
from launch import LaunchContext
import os
from ament_index_python.packages import get_package_share_directory


def shutdown_if_not_shutdown(_, launch_context: LaunchContext):
    if not launch_context.is_shutdown:
        return Shutdown()


def generate_launch_description():

    # world_file_name = 'empty_time_rate.world'
    world_file_name = 'empty_time_rate_iters200.world'

    physic = "x15"  # xN time faster (defined in .world file)
    # physic = "default_physics"
    robot_file_name = 'two_wheel_scar_simple'
    # robot_file_name = 'two_wheel_small_car'

    # Rate of clock messages
    # (https://github.com/ros-simulation/gazebo_ros_pkgs/wiki/
    # ROS-2-Migration:-ROS-Clocks-and-sim-time#clock-rate)
    world_params_file_name = "empty_time_rate.params.yaml"

    world = os.path.join(get_package_share_directory('worm_connectome'), 'worlds', world_file_name)
    world_params = os.path.join(get_package_share_directory('worm_connectome'), 'worlds',
                                world_params_file_name)

    nrun = LaunchConfiguration('nrun')
    stat_range_result_file = LaunchConfiguration('stat_range_result_file')
    odom_result_file = LaunchConfiguration('odom_result_file')
    cells_out_file = LaunchConfiguration('cells_out_file')
    cells_init = LaunchConfiguration('cells_init', default="None")

    return LaunchDescription([
        Node(
            package='worm_connectome',
            node_executable='connectome',
            arguments=[cells_out_file, cells_init],
            node_name='worm',
            parameters=[
                {"use_sim_time": True}
            ],
            output='full',
            emulate_tty=True,
        ),
        Node(
            package='robot_spawner_pkg',
            node_executable='spawn_worm',
            arguments=[robot_file_name, 'worm', '0.0', '0.0', '0.1'],
            node_name='spawner',
            parameters=[
                {"use_sim_time": True}
            ],
            output='screen',
            emulate_tty=True,
        ),
        Node(
            package='worm_connectome',
            node_executable='stat_range',
            arguments=[nrun, stat_range_result_file, odom_result_file],
            node_name='stat_range',
            parameters=[
                {"use_sim_time": True}
            ],
            output='full',
            emulate_tty=True,
            on_exit=shutdown_if_not_shutdown  # Shutdown all when this node ends
        ),
        TimerAction(
            period=2.0,
            actions=[
                ExecuteProcess(
                    cmd=['gzserver', '--verbose', world, '-o',  physic,
                         '-s', 'libgazebo_ros_init.so', "--ros-args", "--params-file",
                         world_params, '-s',  'libgazebo_ros_factory.so'],
                    output='own_log',
                    sigterm_timeout='10',
                    additional_env={"GAZEBO_PLUGIN_PATH": "build/my_diff_drive/"},
                    # on_exit=
                    # emulate_tty=False,
                ),
            ]
        ),
        # RegisterEventHandler(
        #     OnProcessExit(target_action=stat_range_node, on_exit=shutdown_if_not_shutdown)
        # ),
        # RegisterEventHandler(
        #     OnProcessExit(target_action=worm_connectome_node,
        #                   on_exit=(lambda a, b: print(a,b.is_shutdown)))
        # ),
        # RegisterEventHandler(
        #     OnProcessExit(target_action=worm_connectome_node, on_exit=(lambda a, b: Shutdown()))
        # ),
    ])
