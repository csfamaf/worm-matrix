# coding=utf8
# Version with random accumleft and accumright values with normal distribution between
# the possible minimum and maximum accumulated muscle neurons static values

from .gopigo_robot import GoPiGoRobot
from .controller import Controller
from .rand_int import RandInt


# from typing import Dict, Tuple

# disembodied is now class constructor arg
# import argparse
#
# parser = argparse.ArgumentParser()
# parser.add_argument('-d', '--disembodied', help='Run without sensor data', action='store_true')
# parser.add_argument('-v', '--verbose', action='count', default=0)
# # three levels of verbosity: [], -v, -vv
# # 0: only print starting and exiting messages
# # 1: print only when obstacles/food found
# # 2: print speed, left and right every time
# disembodied = parser.parse_args().disembodied
# verbosity = parser.parse_args().verbose
#
# print("Running on Robot: " + str(not disembodied))
#
# if not disembodied:
#     from gopigo import fwd, bwd, left_rot, right_rot, stop, set_speed, us_dist, volt
#
#     from di_sensors.distance_sensor import DistanceSensor  # utilizo el sensor de proximidad
#
#     # print("Example program for reading a Dexter Industries Distance Sensor on an I2C port.")
#
#
# # establish communication with the DistanceSensor
#
#     ds = DistanceSensor()
#
#     # set the sensor in fast-polling-mode
#
#     ds.start_continuous()

###############################################################################
#                                                                             #
#                    Declaracion de Variables y Constantes                    #
#                                                                             #
###############################################################################

# pauso el programa 40 segundos
# print("pausa")
# time.sleep(40)
# Variables:


# Declaro las variables globales que me dan acceso a los distintos estados
# Estas variables serviran como indice de la lista que contiene los valores
# acomulados en cada instante de tiempo (Nota: en python el primer elemento
# de un arreglo es 0)


# The postSynaptic dictionary contains the accumulated weighted values as the
# connectome is executed


# The Threshold is the maximum sccumulated value that must be exceeded before
# the Neurite will fire
threshold = 30

###############################################################################
#                                                                             #
#                       Vector acciones del robot                             #
#                                                                             #
###############################################################################

# creo un vector vacio donde almacenare  las acciones del robot en cada instante
# de tiempo

# se detalla las distintas acciones que puede realizar el roboto y su respectiva
# correspondecia numerica


Detenido = "Detenido"  # el robot esta en estado detenido

# acciones si  nariz fue estimulada
Marcha_atras_izquierda_nariz = "Marcha atras izquierda nariz"  # acciones si nariz fue estimalada
Marcha_atras_nariz = "Marcha atras nariz"  # acciones si nariz fue estimalada
Marcha_atras_derecha_nariz = "Marcha atras derecha nariz"  # acciones si nariz fue estimalada

# acciones si nariz no fue estimulada

# mueve el robot hacia atras y gira la llanta izquierda
Marcha_atras_izquierda = "Marcha atras izquierda"

# el robot retrocede en linea recta
Marcha_atras = "Marcha atras"

# mueve el robot hacia atras y gira la llanta derecha
Marcha_atras_derecha = "Marcha atras derecha"

# mueve el robot hacia adelante y gira la llanta izquierda
Marcha_adelante_izquierda = "Marcha adelante izquierda"

# el robot avanza en linea recta
Marcha_adelante = "Marcha adelante"

# mueve el robot hacia adelante y gira la llanta derecha
Marcha_adelante_derecha = "Marcha adelante derecha"

rotar_derecha = "Rotar derecha"  # el robot rota a la derecha
rotar_izquierda = "Rotar izquierda"  # el robot rota a la izquierda

###############################################################################
#                                                                             #
#             diccionario valores neuronas                                    #
#                                                                             #
###############################################################################


class RandomBlind(Controller):

    min_accumleft = -389
    min_accumright = -391
    max_accumleft = 516
    max_accumright = 512

    def __init__(self, robot: GoPiGoRobot, logger_getter, disembodied=False, verbosity=False,
                 rand_int_class=None, seed=None):

        if not rand_int_class and not issubclass(rand_int_class, RandInt):
            raise TypeError("rand_int_class must be a subclass of RandInt")

        self.seed = seed

        # Fixed seed to do deterministic behavoir

        #############################################################################
        #############################################################################
        #                                                                           #
        #                         Programa principal main()                         #
        #                                                                           #
        #############################################################################
        #############################################################################

        Controller.__init__(self, robot, logger_getter, disembodied=disembodied,
                            verbosity=verbosity, rand_int_class=rand_int_class)

        self.dist = 0

        # Accumulators are used to decide the value to send to the Left and Right motors
        # of the GoPiGo robot
        # Could be local to motorcontrol?
        self.accumleft = 0
        self.accumright = 0

        self.flag_nariz = 0  # First put will initialize again

        if not self.disembodied:
            self.robot.set_speed(120)
            # print("Voltage: ", volt())

        # def main():
        # """Here is where you would put in a method to stimulate the neurons
        #     We stimulate chemosensory neurons constantly unless nose touch
        #     (sonar) is stimulated and then we fire nose touch neurites.
        #     """

        self.tfood = 0

        self.rand_accumleft = rand_int_class(self.min_accumleft, self.max_accumleft, seed=seed)
        self.rand_accumright = rand_int_class(self.min_accumright, self.max_accumright, seed=seed)

    def listener_callback(self, msg):

        # while True:
        if self.tfood < 2:
            if self.verbosity > 0:
                self.get_logger().info("FOOD")

            # en este caso explora el ambiente en busca de comida

            self.flag_nariz = 0

            # Borrado guardado en archivos
            # with open(nombre_neuronas_estimuladas, 'a') as fichero_neuronas_estimuladas:
            #     fichero_neuronas_estimuladas.writelines('{0}\n'.format('hambre'))

            # self.dendriteAccumulate("ADFL")
            # self.dendriteAccumulate("ADFR")
            # self.dendriteAccumulate("ASGR")
            # self.dendriteAccumulate("ASGL")
            # self.dendriteAccumulate("ASIL")
            # self.dendriteAccumulate("ASIR")
            # self.dendriteAccumulate("ASJR")
            # self.dendriteAccumulate("ASJL")
            self.runconnectome()

            self.robot.time_sleep(0.5)

        # if not (0 < self.dist < 300) and tfood >= 2
        # ===> not runconnectome() and not motorcontrol()
        self.tfood += 0.5
        if self.tfood > 20:
            self.tfood = 0

    ###############################################################################
    #                                                                             #
    #                      Procedimiento que corre el conectoma                   #
    #                                                                             #
    ###############################################################################

    # en este procedimiento se corre el conectoma del gusano cuando una cierta cantidad
    # de neuornas se estimula, si alguna neurona dispara se activara el control del
    # robot y volvera a ponerse a cero los pesos acomulados de esta.

    def runconnectome(self):

        self.motorcontrol()

    ###############################################################################
    #                                                                             #
    #              Procedimiento que hace mover el robot                          #
    #                                                                             #
    ###############################################################################

    def motorcontrol(self):
        # global accumright
        # global accumleft

        accion = None

        # accumulate left and right muscles and the accumulated values are
        # used to move the left and right motors of the robot

        # for muscle in self.muscleList:  # if this doesn't work, do muscle in postSynaptic
        #     if muscle in self.mLeft:
        #         self.accumleft += self.postSynaptic[muscle][self.nextState]  # vs thisState???
        #
        #         # print muscle, "Before", postSynaptic[muscle][thisState], accumleft
        #         self.postSynaptic[muscle][self.nextState] = 0
        #         # print muscle, "After", postSynaptic[muscle][thisState], accumleft
        #
        #     elif muscle in self.mRight:
        #         self.accumright += self.postSynaptic[muscle][self.nextState]  # vs thisState???
        #
        #         # postSynaptic[muscle][self.thisState] = 0
        #         self.postSynaptic[muscle][self.nextState] = 0

        self.accumleft = self.rand_accumleft.gen()
        self.accumright = self.rand_accumright.gen()

        # We turn the wheels according to the motor weight accumulation
        new_speed = abs(self.accumleft) + abs(self.accumright)
        if new_speed > 150:
            new_speed = 150
        elif new_speed < 75:
            new_speed = 75

        if not self.disembodied:
            self.robot.set_speed(new_speed)

            # dependiendo de los valores de accumleft y  accumright, el robot hara
            # las siguientes acciones:

            # * Detenido
            # * Mueve el robo hacia atras, existe dos opciones:
            #   * rota a la izquierda
            #   * rota a la derecha
            # * Solo rota a la derecha
            # * Solo rota a la izquierda
            # * Mueve el robo hacia adelante, existe dos opciones:
            #   * rota a la izquierda
            #   * rota a la derecha

            if self.accumleft == 0 and self.accumright == 0:

                accion = Detenido

                self.robot.stop()
            elif self.accumright <= 0 and self.accumleft < 0:
                self.robot.set_speed(150)
                turnratio = float(self.accumright) / float(self.accumleft)
                # print "Turn Ratio: ", turnratio
                if turnratio <= 0.6:

                    # print("Marcha_atras_izquierda")
                    # print("Marcha_atras_izquierda")
                    if self.flag_nariz == 1:

                        accion = Marcha_atras_izquierda_nariz
                    else:

                        accion = Marcha_atras_izquierda
                    # En este estado el robot rota a la izquierda

                    self.robot.left_rot()

                    self.robot.time_sleep(0.8)

                elif turnratio >= 2:

                    # print("Marcha_atras_derecha")

                    if self.flag_nariz == 1:

                        accion = Marcha_atras_derecha_nariz
                    else:

                        accion = Marcha_atras_derecha

                    self.robot.right_rot()
                    self.robot.time_sleep(0.8)

                else:

                    if self.flag_nariz == 1:

                        accion = Marcha_atras_nariz
                    else:

                        accion = Marcha_atras

                self.robot.bwd()
                self.robot.time_sleep(0.5)

            elif self.accumright <= 0 <= self.accumleft:

                # print("rotar derecha")
                # En este estado el robot rota a la derecha
                accion = rotar_derecha

                self.robot.right_rot()
                self.robot.time_sleep(.8)

            elif self.accumright >= 0 >= self.accumleft:

                # En este estado el robot rota a la izquierda

                # print("rotar izquierda")

                accion = rotar_izquierda

                self.robot.left_rot()
                self.robot.time_sleep(.8)

            elif self.accumright >= 0 and self.accumleft > 0:
                turnratio = float(self.accumright) / float(self.accumleft)
                # print "Turn Ratio: ", turnratio
                if turnratio <= 0.6:

                    # print("marcha adelante izquierda")
                    accion = Marcha_adelante_izquierda

                    self.robot.left_rot()
                    self.robot.time_sleep(0.8)
                elif turnratio >= 2:

                    # print("marcha delante derecha")
                    accion = Marcha_adelante_derecha

                    self.robot.right_rot()
                    self.robot.time_sleep(0.8)
                else:

                    accion = Marcha_adelante

                self.robot.fwd()
                self.robot.time_sleep(0.5)

            else:  # Never riched

                # print("detenido")
                accion = Detenido
                self.robot.stop()

        if self.verbosity:
            s = "al: {}, ar: {}, Speed: {}, Action: {}".format(self.accumleft, self.accumright,
                                                               new_speed, accion)
            self.get_logger().info(s)

        self.accumleft = 0
        self.accumright = 0
        # time.sleep(0.5)
