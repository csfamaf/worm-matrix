from .robot import Robot
from abc import abstractmethod

from .sleeper import Sleeper


class HeadedRobot(Robot):

    def __init__(self, sleeper: Sleeper):
        super().__init__(sleeper)

    @abstractmethod
    def head_set_speed(self, speed):
        pass

    @abstractmethod
    def head_left_rot(self):
        pass

    @abstractmethod
    def head_right_rot(self):
        pass

    @abstractmethod
    def head_stop(self):
        pass
