import copy
import json
import os

from ament_index_python.packages import get_package_share_directory
from .rand_int import RandIntUnif


class Synaptics:

    ###############################################################################
    #                                                                             #
    #                 Listas con los nombres de los musculos                      #
    #                                                                             #
    ###############################################################################

    # Se usa para eliminar disparos de Axon ya que los músculos no pueden disparar.

    # The Threshold is the maximum sccumulated value that must be exceeded before
    # the Neurite will fire
    threshold = 30

    musVulvaPrefix = ['MVU']

    musVentralLeftPrefix = ['MVL']
    musDorsalLeftPrefix = ['MDL']
    musLeftPrefix = musVentralLeftPrefix + musDorsalLeftPrefix

    musVentralRightPrefix = ['MVR']
    musDorsalRightPrefix = ['MDR']
    musRightPrefix = musVentralRightPrefix + musDorsalRightPrefix

    musPrefix = musVulvaPrefix + musLeftPrefix + musRightPrefix

    muscleHeadList = [
        'MDL01', 'MDL02', 'MDL03', 'MDL04', 'MDL05', 'MDL06',
        'MVL01', 'MVL02', 'MVL03', 'MVL04', 'MVL05', 'MVL06',
        'MDR01', 'MDR02', 'MDR03', 'MDR04', 'MDR05', 'MDR06',
        'MVR01', 'MVR02', 'MVR03', 'MVR04', 'MVR05', 'MVR06'
    ]

    nose_neurons = ["FLPR", "FLPL", "ASHL", "ASHR", "IL1VL", "IL1VR",
                    "OLQDL", "OLQDR", "OLQVR", "OLQVL"]

    food_neurons = ["ADFL", "ADFR", "ASGR", "ASGL", "ASIL", "ASIR", "ASJR", "ASJL"]

    def __init__(self, ady_matrix_file, logger_getter, inh_scale=None, random_init=False):

        self.mHeadLeft = [m for m in self.muscleHeadList if m.startswith(
            tuple(self.musLeftPrefix))]

        self.mHeadRight = [m for m in self.muscleHeadList if m.startswith(
            tuple(self.musRightPrefix))]

        self.postSynaptic = {}

        self.thisState = 0
        self.nextState = 1
        self.inh_scale = inh_scale

        self.adyMatrix = self.readAdyMatrix(os.path.basename(ady_matrix_file), inh_scale)

        self.get_logger = logger_getter

        # cells lists
        self.cells = None
        self.musBodyDleft = None
        self.musBodyVleft = None
        self.musBodyDright = None
        self.musBodyVright = None
        self.mBodyLeft = None
        self.mBodyRight = None
        self.muscleBodyList = None
        self.musVulva = None
        self.muscles = None
        self.src_neurons = None

        self.fill_cells_lists()

        self.createpostSynaptic(random_init=random_init)

    def readAdyMatrix(self, ady_matrix_file, inh_scale):

        # Opening JSON file
        fullPathFile = os.path.join(get_package_share_directory("worm_connectome"),
                                    "files", ady_matrix_file)

        ady_matrix = {}
        with open(fullPathFile) as json_file:
            ady_matrix = json.load(json_file)

        # Scale inhibitory if requested
        if inh_scale and inh_scale != 1:
            for n_src in ady_matrix.keys():
                for c_tgt in ady_matrix[n_src].keys():
                    v = ady_matrix[n_src][c_tgt]
                    if v < 0:
                        ady_matrix[n_src][c_tgt] = round(inh_scale * v)

        return ady_matrix

    def fill_cells_lists(self):

        self.src_neurons = sorted(list(self.adyMatrix.keys()))
        tgt_cells = [c for tgts in self.adyMatrix.values() for c in tgts.keys()]
        self.cells = sorted(list(set(self.src_neurons + tgt_cells)))

        # Used to accumulate muscle weighted values in body muscles 07-23 = worm locomotion

        self.musBodyDleft = [c for c in self.cells if c.startswith(
            tuple(self.musDorsalLeftPrefix)) and c not in self.muscleHeadList]
        # musBodyDleft = [
        #     'MDL07', 'MDL08', 'MDL09', 'MDL10', 'MDL11', 'MDL12', 'MDL13', 'MDL14', 'MDL15',
        #     'MDL16', 'MDL17', 'MDL18', 'MDL19', 'MDL20', 'MDL21', 'MDL22', 'MDL23', 'MDL24']

        self.musBodyVleft = [c for c in self.cells if c.startswith(
            tuple(self.musVentralLeftPrefix)) and c not in self.muscleHeadList]
        # musBodyVleft = [
        #     'MVL07', 'MVL08', 'MVL09', 'MVL10', 'MVL11', 'MVL12', 'MVL13', 'MVL14', 'MVL15',
        #     'MVL16', 'MVL17', 'MVL18', 'MVL19', 'MVL20', 'MVL21', 'MVL22', 'MVL23']

        self.musBodyDright = [c for c in self.cells if c.startswith(
            tuple(self.musDorsalRightPrefix)) and c not in self.muscleHeadList]
        # musBodyDright = [
        #     'MDR07', 'MDR08', 'MDR09', 'MDR10', 'MDR11', 'MDR12', 'MDR13', 'MDR14', 'MDR15',
        #     'MDR16', 'MDR17', 'MDR18', 'MDR19', 'MDR20', 'MDR21', 'MDR22', 'MDR23', 'MDR24']

        self.musBodyVright = [c for c in self.cells if c.startswith(
            tuple(self.musVentralRightPrefix)) and c not in self.muscleHeadList]
        # musBodyVright = [
        #     'MVR07', 'MVR08', 'MVR09', 'MVR10', 'MVR11', 'MVR12', 'MVR13', 'MVR14', 'MVR15',
        #     'MVR16', 'MVR17', 'MVR18', 'MVR19', 'MVR20', 'MVR21', 'MVR22', 'MVR23', 'MVR24']

        # *Important* #
        # Some muscle list where fixed from the original file by Damian Barsotti
        # muscleBodyList: MDL21 MVL21 where duplicates and there weren't any MDR21 MVR21
        # mBodyRight: there were MDL21 MVL21 but not MDR21 MVL21
        # musBodyDright: there was MDL21 but not MDR21
        # musBodyVright: there was MVL21 but not MVR21

        self.mBodyLeft = self.musBodyVleft + self.musBodyDleft

        self.mBodyRight = self.musBodyVright + self.musBodyDright

        # With Timothy error:
        # mBodyRight = [
        #     'MDR07', 'MDR08', 'MDR09', 'MDR10', 'MDR11', 'MDR12', 'MDR13', 'MDR14', 'MDR15',
        #     'MDR16', 'MDR17', 'MDR18', 'MDR19', 'MDR20', 'MDL21', 'MDR22', 'MDR23', 'MVR07',
        #     'MVR08', 'MVR09', 'MVR10', 'MVR11', 'MVR12', 'MVR13', 'MVR14', 'MVR15', 'MVR16',
        #     'MVR17', 'MVR18', 'MVR19', 'MVR20', 'MVL21', 'MVR22', 'MVR23'
        # ]

        # There aren't MDL24 MDR24 MVR24 in the original program muscle list
        # (worm doesn't have MVL24)
        # Damian fixed this)
        self.muscleBodyList = self.mBodyLeft + self.mBodyRight

        # With Timothy error:
        # muscleBodyList = [
        #     'MDL07', 'MDL08', 'MDL09', 'MDL10', 'MDL11', 'MDL12', 'MDL13', 'MDL14', 'MDL15',
        #     'MDL16', 'MDL17', 'MDL18', 'MDL19', 'MDL20', 'MDL21', 'MDL22', 'MDL23', 'MVL07',
        #     'MVL08', 'MVL09', 'MVL10', 'MVL11', 'MVL12', 'MVL13', 'MVL14', 'MVL15', 'MVL16',
        #     'MVL17', 'MVL18', 'MVL19', 'MVL20', 'MVL21', 'MVL22', 'MVL23', 'MDR07', 'MDR08',
        #     'MDR09', 'MDR10', 'MDR11', 'MDR12', 'MDR13', 'MDR14', 'MDR15', 'MDR16', 'MDR17',
        #     'MDR18', 'MDR19', 'MDR20', 'MDL21', 'MDR22', 'MDR23', 'MVR07', 'MVR08', 'MVR09',
        #     'MVR10', 'MVR11', 'MVR12', 'MVR13', 'MVR14', 'MVR15', 'MVR16', 'MVR17', 'MVR18',
        #     'MVR19', 'MVR20', 'MVL21', 'MVR22', 'MVR23'
        # ]

        self.musVulva = [c for c in self.cells if c.startswith(tuple(self.musVulvaPrefix))]

        self.muscles = self.muscleBodyList + self.muscleHeadList + self.musVulva

        self.neurons = [c for c in self.cells if c not in self.muscles]
        # Not muscles. Doesn't star with muscles_prefix. There are 301
        # neurons = ['ADAL', 'ADAR', 'ADEL', 'ADER', 'ADFL', 'ADFR', 'ADLL', 'ADLR', 'AFDL',
        #            'AFDR', 'AIAL', 'AIAR', 'AIBL', 'AIBR', 'AIML', 'AIMR', 'AINL', 'AINR',
        #            'AIYL', 'AIYR', 'AIZL', 'AIZR', 'ALA', 'ALML', 'ALMR', 'ALNL', 'ALNR',
        #            'AQR', 'AS1', 'AS10', 'AS11', 'AS2', 'AS3', 'AS4', 'AS5', 'AS6', 'AS7',
        #            'AS8', 'AS9', 'ASEL', 'ASER', 'ASGL', 'ASGR', 'ASHL', 'ASHR', 'ASIL',
        #            'ASIR', 'ASJL', 'ASJR', 'ASKL', 'ASKR', 'AUAL', 'AUAR', 'AVAL', 'AVAR',
        #            'AVBL', 'AVBR', 'AVDL', 'AVDR', 'AVEL', 'AVER', 'AVFL', 'AVFR', 'AVG',
        #            'AVHL', 'AVHR', 'AVJL', 'AVJR', 'AVKL', 'AVKR', 'AVL', 'AVM', 'AWAL',
        #            'AWAR', 'AWBL', 'AWBR', 'AWCL', 'AWCR', 'BAGL', 'BAGR', 'BDUL', 'BDUR',
        #            'CEPDL', 'CEPDR', 'CEPVL', 'CEPVR', 'DA1', 'DA2', 'DA3', 'DA4', 'DA5',
        #            'DA6', 'DA7', 'DA8', 'DA9', 'DB1', 'DB2', 'DB3', 'DB4', 'DB5', 'DB6',
        #            'DB7', 'DD1', 'DD2', 'DD3', 'DD4', 'DD5', 'DD6', 'DVA', 'DVB', 'DVC',
        #            'FLPL', 'FLPR', 'HSNL', 'HSNR', 'I1L', 'I1R', 'I2L', 'I2R', 'I3', 'I4',
        #            'I5', 'I6', 'IL1DL', 'IL1DR', 'IL1L', 'IL1R', 'IL1VL', 'IL1VR', 'IL2L',
        #            'IL2R', 'IL2DL', 'IL2DR', 'IL2VL', 'IL2VR', 'LUAL', 'LUAR', 'M1', 'M2L',
        #            'M2R', 'M3L', 'M3R', 'M4', 'M5', 'MANAL', 'MCL', 'MCR', 'MI', 'NSML',
        #            'NSMR', 'OLLL', 'OLLR', 'OLQDL', 'OLQDR', 'OLQVL', 'OLQVR', 'PDA', 'PDB',
        #            'PDEL', 'PDER', 'PHAL', 'PHAR', 'PHBL', 'PHBR', 'PHCL', 'PHCR', 'PLML',
        #            'PLMR', 'PLNL', 'PLNR', 'PQR', 'PVCL', 'PVCR', 'PVDL', 'PVDR', 'PVM',
        #            'PVNL', 'PVNR', 'PVPL', 'PVPR', 'PVQL', 'PVQR', 'PVR', 'PVT', 'PVWL',
        #            'PVWR', 'RIAL', 'RIAR', 'RIBL', 'RIBR', 'RICL', 'RICR', 'RID', 'RIFL',
        #            'RIFR', 'RIGL', 'RIGR', 'RIH', 'RIML', 'RIMR', 'RIPL', 'RIPR', 'RIR',
        #            'RIS', 'RIVL', 'RIVR', 'RMDDL', 'RMDDR', 'RMDL', 'RMDR', 'RMDVL',
        #            'RMDVR', 'RMED', 'RMEL', 'RMER', 'RMEV', 'RMFL', 'RMFR', 'RMGL', 'RMGR',
        #            'RMHL', 'RMHR', 'SAADL', 'SAADR', 'SAAVL', 'SAAVR', 'SABD', 'SABVL',
        #            'SABVR', 'SDQL', 'SDQR', 'SIADL', 'SIADR', 'SIAVL', 'SIAVR', 'SIBDL',
        #            'SIBDR', 'SIBVL', 'SIBVR', 'SMBDL', 'SMBDR', 'SMBVL', 'SMBVR', 'SMDDL',
        #            'SMDDR', 'SMDVL', 'SMDVR', 'URADL', 'URADR', 'URAVL', 'URAVR', 'URBL',
        #            'URBR', 'URXL', 'URXR', 'URYDL', 'URYDR', 'URYVL', 'URYVR', 'VA1', 'VA10',
        #            'VA11', 'VA12', 'VA2', 'VA3', 'VA4', 'VA5', 'VA6', 'VA7', 'VA8', 'VA9',
        #            'VB1', 'VB10', 'VB11', 'VB2', 'VB3', 'VB4', 'VB5', 'VB6', 'VB7', 'VB8',
        #            'VB9', 'VC1', 'VC2', 'VC3', 'VC4', 'VC5', 'VC6', 'VD1', 'VD10', 'VD11',
        #            'VD12', 'VD13', 'VD2', 'VD3', 'VD4', 'VD5', 'VD6', 'VD7', 'VD8', 'VD9']

    ###############################################################################
    #                                                                             #
    #                             Conectoma completo                              #
    #                                                                             #
    ###############################################################################

    """This is the full C Elegans Connectome as expresed in the form of the Presynatptic
    neurite and the self.postSynaptic neurites.

    self.postSynaptic['ADAR'][self.nextState] = (2 + self.postSynaptic['ADAR'][thisState])
    arr=self.postSynaptic['AIBR'] potential optimization
    """

    ###############################################################################
    #                                                                             #
    #                         Declaracion de estructuras                          #
    #                                                                             #
    ###############################################################################
    #                                                                             #
    #             diccionario self.postSynaptic                                        #
    #                                                                             #
    ###############################################################################

    # se crea el diccionario llamado self.postSynaptic, para ellos crea
    # entradas (keys) al diccionario vacio creado anteriormente, los valores
    # asociados a cada entrada (key) son una lista de dos elementos, en donde se
    # se almacenaran en cada instante de tiempo el valor acomulado del estado actual
    # y del siguiente estado.

    # inicialmente esta iniciado a cero.

    # para crear una entrada al diccionario se utiliza:
    # self.postSynaptic['Neurona'] = [0,0]

    # La estructura creada por este procedimiento es de la forma:

    # { 'key'   : value(lista),

    #    'AVAL'  : [0,0] ,

    #    'AVAR'  : [0,0] ,

    #            .
    #            .
    #            .

    #    'VDA'   : [0.0]

    #    'musculo1' : [0.0]

    #  }

    # Nota: Este diccionario no solo contiene las neuronas si no tambien la
    # acomulacion de los musculos cuando una neurona se activa

    def createpostSynaptic(self, random_init=False):
        # The self.postSynaptic dictionary maintains the accumulated values for
        # each neuron and muscle. The Accumulated values are initialized to Zero
        if not random_init:
            self.get_logger().info("Zero initial conectome")
            for c in self.cells:
                self.postSynaptic[c] = [0, 0]
        else:
            self.get_logger().info("Random initial conectome")
            rnd = RandIntUnif(0, self.threshold)
            for c in self.cells:
                r = rnd.gen()
                self.postSynaptic[c] = [r, r]

    ###############################################################################
    #                                                                             #
    #           Funciones que emulan la actividad neuronal del conectoma          #
    #                                                                             #
    ###############################################################################

    def __fireNeuron(self, fneuron):
        for n, v in self.adyMatrix[fneuron].items():
            self.postSynaptic[n][self.nextState] += v

    def dendriteAccumulate(self, dneuron):
        # f = getattr(self, dneuron)
        # f()
        self.__fireNeuron(dneuron)

    def fireNeuron(self, fneuron):
        # The threshold has been exceeded and we fire the neurite
        # print(thisState, self.nextState)

        if fneuron != "MVULVA":
            # f = getattr(self, fneuron)
            # f()
            self.__fireNeuron(fneuron)
            self.postSynaptic[fneuron][self.nextState] = 0

    def fire_neurons(self):
        for ps in self.src_neurons:
            if abs(self.postSynaptic[ps][self.thisState]) > self.threshold:
                self.fireNeuron(ps)

    # def fire_neurons(self):
    #     for ps in self.postSynaptic:
    #         if (ps[:3] not in self.musPrefix and
    #                 abs(self.postSynaptic[ps][self.thisState]) > self.threshold):
    #             self.fireNeuron(ps)

    def stimulate_nose(self):
        for n in self.nose_neurons:
            self.dendriteAccumulate(n)

    def stimulate_food(self):
        for n in self.food_neurons:
            self.dendriteAccumulate(n)

    def swapStates(self):

        # Copy nextState values to thisState
        for ps in self.postSynaptic:
            # if postSynaptic[ps][thisState] != 0:
            #         print ps
            #         print "Before Clone: ", postSynaptic[ps][thisState]

            # fired neurons keep getting reset to previous weight
            # wtf deepcopy -- So, the concern is that the deepcopy doesnt
            # scale up to larger neural networks??
            self.postSynaptic[ps][self.thisState] = copy.deepcopy(
                self.postSynaptic[ps][self.nextState])

            # this deep copy is not in the functioning version currently.
            # print "After Clone: ", postSynaptic[ps][thisState]

        # Swap state index (it isn't necesary)
        self.thisState, self.nextState = self.nextState, self.thisState
