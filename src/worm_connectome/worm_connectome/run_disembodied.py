from enum import Enum, unique
import sys
import argparse
from math import sqrt, sin, cos, pi
import time
import csv
from statistics import mean, stdev
from typing import Tuple

from .robot import Robot
from .sleeper import Sleeper
from .connectome import Connectome
from .rand_int import set_seed

SEED_DEFAULT = 2321

MAX_DIST_DEFAULT = 30.0

NANO_IN_SEC = 10 ** 9


@unique
class Move(Enum):
    fwd = 1
    bwd = 2
    left_rot = 3
    right_rot = 4
    stop = 5
    none = 6


def sec_to_nano(secs: float):
    return round(secs*NANO_IN_SEC)


def nano_to_sec(nanos: int):
    return nanos / NANO_IN_SEC  # float division in python 3


def nano_to_sec_nano(nanos: int):
    return nanos // NANO_IN_SEC, nanos % NANO_IN_SEC


class SleeperRobot(Sleeper, Robot):

    time_factor = 1
    # time_factor = 0.9
    # time_factor = 1.176

    def __init__(self, csvfile, time_nano_init=0, x=0.0, y=0.0, theta=0.0,
                 max_time_sec=None, max_dist=MAX_DIST_DEFAULT, run_time_connectome=False):

        Robot.__init__(self, self)  # Pass itself since is a Sleeper

        self.__nanosecs = time_nano_init
        self.last_sleep = 0.0
        self.speed = 0.0
        self.x = x
        self.y = y
        self.theta = theta
        self.max_time_sec = max_time_sec
        self.max_dist = max_dist
        self.run_time_connectome = run_time_connectome

        self.last_move = Move.stop

        self.__update_pos = {
            Move.fwd: self.__fwd,
            Move.bwd: self.__bwd,
            Move.left_rot: self.__left_rot,
            Move.right_rot: self.__right_rot,
            Move.stop: self.__stop,
        }

        self.real_time = time.time()

        fieldnames = ['sec', 'nanosec', 'x', 'y', 'theta']
        self.writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        self.writer.writeheader()
        self.writerow()

        self.runc_times = []

    def writerow(self):
        sec, nanosec = nano_to_sec_nano(self.nanosecs)

        self.writer.writerow(
            {'sec': sec, 'nanosec': nanosec, 'x': self.x, 'y': self.y, 'theta': self.theta})

    @property
    def nanosecs(self):
        return self.__nanosecs

    # @time.setter
    # def time(self, value):
    #     print("Time:", value)
    #     self.__nanosecs = value

    def update_pos_time(self):
        self.__update_pos[self.last_move]()
        self.__nanosecs += sec_to_nano(self.last_sleep)
        self.writerow()

    def sleep(self, secs: float):
        self.add_time_runconnectome()
        print("Sleep:", secs,)
        self.last_sleep = secs * self.time_factor
        self.update_pos_time()

    def set_speed(self, speed: int):
        self.speed = self.speed_factor * abs(speed)

    def fwd(self):
        self.add_time_runconnectome()
        self.last_move = Move.fwd

    def bwd(self):
        self.add_time_runconnectome()
        self.last_move = Move.bwd

    def left_rot(self):
        self.add_time_runconnectome()
        self.last_move = Move.left_rot

    def right_rot(self):
        self.add_time_runconnectome()
        self.last_move = Move.right_rot

    def stop(self):
        self.add_time_runconnectome()
        self.last_move = Move.stop

    def ds_read(self, _):
        """Convert msg.Range to mm."""
        self.real_time = time.time()  # Set to test time spend by runconnectome

        return sys.maxsize

    def add_time_runconnectome(self):
        if self.run_time_connectome and self.real_time:  # not None = Time passed
            # self.last_sleep = 0.06  # (0,0006/2,4e-06)*0,00024051623009229735
            self.last_sleep = (time.time() - self.real_time) * self.time_factor
            self.runc_times.append(self.last_sleep)
            self.update_pos_time()
            # print("runconnectome time:", self.last_sleep)
            self.real_time = None

    def dist(self):
        return sqrt(self.x**2 + self.y**2)

    def done(self):
        if self.max_time_sec is not None:
            return nano_to_sec(self.nanosecs) >= self.max_time_sec
        else:
            return self.dist() >= self.max_dist

    def __fwd(self):
        r = self.last_sleep * self.speed
        self.x += r * cos(self.theta)
        self.y += r * sin(self.theta)
        print("Fwd speed: {}, time: {}, theta: {}".format(self.speed, self.last_sleep, self.theta))

    def __bwd(self):
        r = self.last_sleep * self.speed
        self.x += r * cos(self.theta + pi)
        self.y += r * sin(self.theta + pi)
        print("Bwd speed: {}, time: {}, theta: {}".format(self.speed, self.last_sleep, self.theta))

    def __left_rot(self):
        vang = self.angular_factor * self.speed
        ang = vang * self.last_sleep
        print("Z ang speed: {}, time: {}, theta: {}".format(vang, self.last_sleep, self.theta))
        self.theta += ang

    def __right_rot(self):
        vang = - self.angular_factor * self.speed
        ang = vang * self.last_sleep
        print("Z ang speed: {}, time: {}, theta: {}".format(vang, self.last_sleep, self.theta))
        self.theta += ang

    def __stop(self):
        print("Stop, time: {}, theta: {}".format(self.last_sleep, self.theta))

    def get_time(self) -> Tuple[int, int]:
        return nano_to_sec_nano(self.nanosecs)


class Info:
    @staticmethod
    def info(s: str):
        print(s)


def get_logger():
    info = Info()
    return info


def run_disembodied(file_path_name, file_out_cells, file_init_connectome=None,
                    ady_matrix_file=None, random_init=False, max_time_sec=None):

    with open(file_path_name, 'w', newline='') as csvpath_file:

        robot_sleeper = SleeperRobot(csvpath_file, max_time_sec=max_time_sec)

        connectome = Connectome(robot_sleeper, get_logger, verbosity=2,
                                file_init_connectome=file_init_connectome,
                                file_out_cells=file_out_cells,
                                ady_matrix_file=ady_matrix_file,
                                random_init=random_init)

        while not robot_sleeper.done():
            connectome.listener_callback(None)
            print(robot_sleeper.dist(), nano_to_sec(robot_sleeper.nanosecs))

        if robot_sleeper.run_time_connectome:
            print("Average runconnectome times: {} +- {}".format(
                mean(robot_sleeper.runc_times),
                stdev(robot_sleeper.runc_times)))
            print("Max: {}, Min: {}".format(max(robot_sleeper.runc_times),
                                            min(robot_sleeper.runc_times)))


def main():

    parser = argparse.ArgumentParser()

    parser.add_argument("path_out", help="Output path filename")
    parser.add_argument("cells_out", help="File name output cells value through time")
    parser.add_argument("-ci", "--cells-init", help="File name with connectome initialization")
    parser.add_argument("-m", "--ady-matrix",
                        help="File name with connectome connectome ady matrix. Default Timothy")
    parser.add_argument("-ri", "--random-init", choices=('yes', 'no'), default='no',
                        help=(
                            "Random connectome initialization. "
                            "Default no (conectome is initialized to 0)"))
    parser.add_argument("-s", "--seed", type=int,
                        help=(f"Random seed. Default {SEED_DEFAULT}"))
    parser.add_argument("-mt", "--max-time-sec", type=int,
                        help=("Max simulation time in seconds."
                              f" Default use max dist to {MAX_DIST_DEFAULT}"))
    parser.add_argument("--ros-args", action="store_true", help="ROS argument")
    parser.add_argument("-r", help="ROS argument")

    args = parser.parse_args()

    # if len(sys.argv) != 3 and len(sys.argv) != 4:
    #     print("Usage: ros2 run worm_connectome disembodied"
    #           " <file path> <file_out_cells> [<file cells init>]",
    #           file=sys.stderr)
    #     sys.exit(2)

    if args.seed is None:
        set_seed(SEED_DEFAULT)
        print("Seed set to Default")
    else:
        set_seed(args.seed)
        print("Seed set to "+str(args.seed))

    run_disembodied(args.path_out, args.cells_out, file_init_connectome=None,
                    ady_matrix_file=args.ady_matrix,
                    random_init=args.random_init == 'yes',
                    max_time_sec=args.max_time_sec)


if __name__ == '__main__':
    main()
