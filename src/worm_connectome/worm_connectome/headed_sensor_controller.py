"""Class to implement sensor-controller-actuator model."""

from rclpy.node import Node

from rclpy.qos import QoSHistoryPolicy, qos_profile_sensor_data

from sensor_msgs.msg import Range
from geometry_msgs.msg import Twist

from lifecycle_msgs.srv import ChangeState
from lifecycle_msgs.msg import Transition

from asyncio import Future

from .headed_gopigo_robot import HeadedGoPiGoRobot
from .headed_controller import HeadedController

from .sleeper import Sleeper


class HeadedSensorController(Node):
    """
    Class to implement sensor-controller-actuator model.

    Derived from Node to implement a channel sensor as a subscriber and a channel actuator as a
    publisher.
    The execution is activated by the controller's method callback that listen on the susbcribed
    sensor channel.
    The robot acts as interface used by a controller (connectome or teleop) to send commands
    """

    def __init__(self, controller_class, sleeper: Sleeper, verbosity=False, speed=None, **kwargs):

        if not issubclass(controller_class, HeadedController):
            raise TypeError("controller_class must be a subclass of Controller")

        super().__init__('worm')

        self.verbosity = verbosity

        body_publisher = self.create_publisher(Twist, 'worm/cmd_worm', 10)
        head_publisher = self.create_publisher(Twist, 'worm/cmd_head', 10)

        if speed:
            robot = HeadedGoPiGoRobot(body_publisher, head_publisher, sleeper,
                                      self.get_logger, verbosity=verbosity, speed=speed)
        else:
            robot = HeadedGoPiGoRobot(body_publisher, head_publisher, sleeper,
                                      self.get_logger, verbosity=verbosity)

        self.controller = controller_class(robot, self.get_logger, verbosity=verbosity, **kwargs)

        qos_profile = qos_profile_sensor_data
        qos_profile.depth = 1
        qos_profile.history = QoSHistoryPolicy.RMW_QOS_POLICY_HISTORY_KEEP_LAST

        # qos_profile = QoSProfile(depth=1,
        #                          history=QoSHistoryPolicy.RMW_QOS_POLICY_HISTORY_KEEP_LAST)

        self.subscription = self.create_subscription(
            Range,
            'worm/laser_scan',
            self.controller.listener_callback,
            qos_profile)

        # Create service for shutdown only
        self.change_state_srv = self.create_service(
            ChangeState,
            self.get_name()+'/change_state',
            self.change_state_callback)

        self.done = Future()

        # self.get_logger().info('Ceate serv')

        self.get_logger().info('Begin')
        # self.subscription  # prevent unused variable warning

    def change_state_callback(self, request, response):
        if request.transition.id == Transition.TRANSITION_DESTROY:
            self.get_logger().info('Destroyed')
            self.done.set_result(True)
            # self.destroy_node() doesn't work: throw exception

        response.success = True
        return response
