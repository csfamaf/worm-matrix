import rclpy
from rclpy.executors import SingleThreadedExecutor
from ament_index_python.packages import get_package_share_directory
import threading
import argparse
from os import path

from .sensor_controller import SensorController
from .connectome import Connectome
from .sleeper import NodeSleeper
from .rand_int import set_seed

SEED_DEFAULT = 2321


def main(args=None):

    rclpy.init(args=args)

    parser = argparse.ArgumentParser()

    parser.add_argument("-co", "--cells-out", help="File name output cells value through time")
    parser.add_argument("-ci", "--cells-init", help="File name with connectome initialization")
    parser.add_argument("-m", "--ady-matrix",
                        help="File name with connectome connectome ady matrix. Default Timothy")
    parser.add_argument("-is", "--inh-scale", type=float,
                        help="Scale for inhibitory connection. Default 1.0")
    parser.add_argument("-md", "--max-dist-collision", type=int,
                        help=("Max distance of collision detection." +
                              f"Default {Connectome.max_dist_collision_default}"))
    parser.add_argument("-lsf", "--linear-speed-factor", type=float,
                        help=("Forward-backward speed factor (not rotation)." +
                              f"Default 1.0"))
    parser.add_argument("-ri", "--random-init", choices=('yes', 'no'), default='no',
                        help=(
                            "Random connectome initialization. "
                            "Default no (conectome is initialized to 0)"))
    parser.add_argument("-s", "--seed", type=int,
                        help=(f"Random seed. Default {SEED_DEFAULT}"))
    parser.add_argument("--ros-args", action="store_true", help="ROS argument")
    parser.add_argument("--params-file", help="ROS argument")
    parser.add_argument("-r", help="ROS argument")

    args = parser.parse_args()

    executor = SingleThreadedExecutor()

    sleeper = NodeSleeper(verbosity=True)

    executor.add_node(sleeper)
    thread = threading.Thread(target=executor.spin, args=(), daemon=True)
    thread.start()

    sleeper.wait_ready()

    if args.cells_init:
        file_cells_init_name = path.join(
            get_package_share_directory("worm_connectome"), "files", args.cells_init
        )
    else:
        file_cells_init_name = None

    if args.seed is None:
        set_seed(SEED_DEFAULT)
        print("Seed set to Default")
    else:
        set_seed(args.seed)
        print("Seed set to "+str(args.seed))

    connectome_worm = SensorController(Connectome, sleeper, verbosity=True,
                                       file_out_cells=args.cells_out,
                                       file_init_connectome=file_cells_init_name,
                                       ady_matrix_file=args.ady_matrix,
                                       inh_scale=args.inh_scale,
                                       max_dist_collision=args.max_dist_collision,
                                       linear_speed_factor=args.linear_speed_factor,
                                       random_init=args.random_init == 'yes')

    print('To finish the controller do:',
          'ros2 service call /worm/change_state lifecycle_msgs/srv/ChangeState'
          ' "{transition: {id: 8}}"')

    try:
        # Run until Future by receiving "Destroy" with service
        rclpy.spin_until_future_complete(connectome_worm, connectome_worm.done)
    except KeyboardInterrupt:
        # Destroy the node explicitly
        # (optional - otherwise it will be done automatically
        # when the garbage collector destroys the node object)
        # connectome_worm.destroy_node()
        # rclpy.shutdown()
        print("Interrupted")
        # sys.exit()

    sleeper.destroy_node()
    connectome_worm.destroy_node()
    rclpy.shutdown()
    print("Done")


if __name__ == '__main__':
    main()
