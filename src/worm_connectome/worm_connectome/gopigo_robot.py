from geometry_msgs.msg import Twist

from .sleeper import Sleeper
from .robot import Robot


class GoPiGoRobot(Robot):

    def __init__(
        self, publisher, sleeper: Sleeper,
        logger_getter, verbosity=0, speed=0,
        linear_speed_factor=None  # Only forward-backward speed factor (not rotation)
    ):

        super().__init__(sleeper)

        self.verbosity = verbosity
        self.motor = publisher
        self.sleeper = sleeper
        self.get_logger = logger_getter

        if linear_speed_factor is None:
            self.linear_speed_factor = 1.0
        else:
            self.linear_speed_factor = linear_speed_factor
            self.get_logger().info(f"linear_speed_factor={self.linear_speed_factor}")

        self._speed = 0.0
        self._tw = Twist()
        self.zero_twist()

        if speed is None:
            self.set_speed(0)
        else:
            self.set_speed(speed)

    def zero_twist(self):
        self._tw.linear.y = 0.0
        self._tw.linear.z = 0.0
        self._tw.angular.y = 0.0
        self._tw.angular.x = 0.0

        self._tw.linear.x = 0.0  # meter per second
        self._tw.angular.z = 0.0  # radians per second

    def send_command(self):
        self.motor.publish(self._tw)
        if self.verbosity:
            self.get_logger().info(
                "Twist (x,z) = ({:.2f},{:.2f})".format(self._tw.linear.x, self._tw.angular.z)
            )

    def set_speed(self, speed):
        self._speed = self.speed_factor * abs(speed)
        if self.verbosity > 2:
            self.get_logger().info(
                "Speed (Controller, Simul) = ({},{:.2f})".format(speed, self._speed)
            )

    def fwd(self):
        self.zero_twist()
        self._tw.linear.x = self._speed * self.linear_speed_factor
        self.send_command()

    def bwd(self):
        self.zero_twist()
        self._tw.linear.x = - self._speed * self.linear_speed_factor
        self.send_command()

    def left_rot(self):
        self.zero_twist()
        self._tw.angular.z = self.angular_factor * self._speed
        self.send_command()

    def right_rot(self):
        self.zero_twist()
        self._tw.angular.z = -self.angular_factor * self._speed
        self.send_command()

    def stop(self):
        self.zero_twist()
        self.send_command()

    def ds_read(self, msg):
        """Convert msg.Range to mm."""
        if self.verbosity > 1:
            self.get_logger().info("Sensor Range: {:.2f}".format(msg.range))

        return msg.range * 1000
        # return round(min((msg.range-0.012)*1000, sys.maxsize))
        # return 10000
        # if np.random.randint(2):
        #     return 10000
        # else:
        #     return 10
