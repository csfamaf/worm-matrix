import scipy.stats as ss
import numpy as np

from abc import ABC, abstractmethod

_rng = None


def set_seed(seed):
    global _rng

    _rng = np.random.default_rng(seed=seed)


class RandInt(ABC):

    @abstractmethod
    def __init__(self, minimum, maximum, seed=None):
        global _rng
        self.minimum = minimum
        self.maximum = maximum
        self.seed = seed
        # Initialized only one time
        if _rng is None:
            _rng = np.random.default_rng(seed=seed)

    @abstractmethod
    def gen(self):
        pass


class RandIntNorm(RandInt):
    """Normal (Gaussian) discrete integer random number generator."""

    def __init__(self, minimum, maximum, seed=None):

        super().__init__(minimum, maximum, seed=seed)

        ss.random_state = _rng

        self.loc = (maximum + minimum)/2

        # 3 sigma to reach maximum and minimum
        self.scale = round((maximum - minimum)/6)

        self.x = np.arange(minimum, maximum+1)

        self.xU, self.xL = self.x + 0.5, self.x - 0.5

        prob = ss.norm.pdf(self.x, scale=self.scale, loc=self.loc)
        self.prob = prob / prob.sum()

        print("min: {}, max: {}, loc: {}, scale{}".format(minimum, maximum, self.loc, self.scale))

    def __gen(self):
        return _rng.choice(self.x, p=self.prob)

    def gen(self):

        res = self.__gen()

        while not (self.minimum <= res <= self.maximum):
            res = self.__gen()

        return res


class RandIntUnif(RandInt):
    """Uniform discrete integer random number generator."""

    def __init__(self, minimum, maximum, seed=None):

        super().__init__(minimum, maximum, seed=seed)

    def gen(self):
        return _rng.integers(self.minimum, self.maximum+1)
