from abc import ABC, abstractmethod

from rclpy.node import Node

import time
from typing import Tuple

NANO_IN_SEC = 10 ** 9

MULT_TIME = 1


class Sleeper(ABC):

    @abstractmethod
    def sleep(self, secs: float):
        pass

    @abstractmethod
    def get_time(self) -> Tuple[int, int]:
        """Return (second, nanoseconds)."""
        pass


class NodeSleeper(Node, Sleeper):
    """Sim Time Sleeper."""

    def __init__(self, verbosity=False):

        Node.__init__(self, "sleeper")

        self.verbosity = verbosity

    def wait_ready(self):

        if self.verbosity:
            self.get_logger().info("Begin sleeper.wait_ready")

        while self.get_clock().now().nanoseconds == 0:
            time.sleep(0.5)

        if self.verbosity:
            self.get_logger().info("End sleeper.wait_ready")

    def sleep(self, secs):

        ftime = round(secs * NANO_IN_SEC * MULT_TIME)

        stime = self.get_clock().now().nanoseconds

        while self.get_clock().now().nanoseconds - stime < ftime:
            time.sleep(0.005)

        if self.verbosity:
            self.get_logger().info("[sleeper]: request={}, give={}".format(
                secs,
                (self.get_clock().now().nanoseconds - stime) / NANO_IN_SEC
            ))

    # def sleep(self, secs):
    #     stime = 0
    #
    #     rate = self.create_rate(1.0/secs)
    #
    #     if self.verbosity:
    #         stime = self.get_clock().now().nanoseconds
    #
    #     rate.sleep()
    #
    #     if self.verbosity:
    #         self.get_logger().info("[sleeper]: request={}, give={}".format(
    #             secs,
    #             (self.get_clock().now().nanoseconds - stime) / 1000000000
    #         ))
    #
    #     rate.destroy()

    def get_time(self):
        return self.get_clock().now().seconds_nanoseconds()
