import rclpy
from rclpy.executors import SingleThreadedExecutor
import threading

import sys
import select
import tty
import termios

from .headed_sensor_controller import HeadedSensorController
from .headed_controller import HeadedController
from .headed_gopigo_robot import HeadedGoPiGoRobot
from .sleeper import NodeSleeper


class KeyReader(HeadedController):

    HIGH_SPEED = 150
    LOW_SPEED = 75

    def __init__(self, robot: HeadedGoPiGoRobot, logger_getter, verbosity=False):

        if not isinstance(robot, HeadedGoPiGoRobot):
            raise TypeError("robot must be an instance of GoPiGoRobot")

        HeadedController.__init__(self, robot, logger_getter, verbosity=verbosity)

        self.robot = robot
        self.verbosity = verbosity
        self.get_logger = logger_getter
        self.termios_old_settings = termios.tcgetattr(sys.stdin)

        self.last_range = 0.0

        tty.setcbreak(sys.stdin.fileno())

        self.robot.head_set_speed(self.HIGH_SPEED)

    @staticmethod
    def is_data():
        return select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], [])

    def listener_callback(self, msg):

        if self.is_data():

            k = sys.stdin.read(1)

            if k == '\x1b':  # x1b is ESC
                k += sys.stdin.read(2)

            if k == '\x1b[A':  # up arrow
                self.robot.fwd()
            elif k == '\x1b[B':  # down arrow
                self.robot.bwd()
            elif k == '\x1b[C':  # right arrow
                self.robot.right_rot()
            elif k == '\x1b[D':  # left arrow
                self.robot.left_rot()
            elif k == '>':  # high speed
                self.robot.set_speed(self.HIGH_SPEED)
            elif k == '<':  # low speed
                self.robot.set_speed(self.LOW_SPEED)
            elif k == 'd':  # right arrow
                self.robot.head_right_rot()
            elif k == 'a':  # left arrow
                self.robot.head_left_rot()
            elif k == 'q':  # high speed
                self.robot.head_set_speed(self.HIGH_SPEED)
            elif k == 'z':  # low speed
                self.robot.head_set_speed(self.LOW_SPEED)
            elif k == 's':  # left arrow
                self.robot.head_stop()
            else:  # stop
                self.robot.stop()

        if self.verbosity > 1:
            if msg.range != self.last_range:
                self.last_range = msg.range
                self.get_logger().info("Sensor Range: {:.2f}".format(msg.range))

    def __del__(self):
        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, self.termios_old_settings)


def main(args=None):
    rclpy.init(args=args)

    executor = SingleThreadedExecutor()

    sleeper = NodeSleeper(verbosity=True)

    executor.add_node(sleeper)
    thread = threading.Thread(target=executor.spin, args=(), daemon=True)
    thread.start()

    sleeper.wait_ready()

    teleop_worm = HeadedSensorController(KeyReader, sleeper, verbosity=2,
                                         speed=KeyReader.HIGH_SPEED)

    try:
        rclpy.spin(teleop_worm)
    except KeyboardInterrupt:
        # Destroy the node explicitly
        # (optional - otherwise it will be done automatically
        # when the garbage collector destroys the node object)
        sleeper.destroy_node()
        teleop_worm.destroy_node()
        rclpy.shutdown()
        print("Bye")
        sys.exit()


if __name__ == '__main__':
    main()
