import csv
import numpy as np
import sys

from worm_connectome.synaptics import Synaptics

# seed = 123

min_neuron_value = 0
max_neuron_value = 1


def write_csv_from_dicts(dct):

    csvfile = sys.stdout
    fieldnames = list(dct.keys())
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    writer.writerow(dct)


def read_zero_neurons(fzero_neurons):

    with open(fzero_neurons, 'r', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        r = next(reader)
        for k in r.keys():
            r[k] = int(r[k])

        return r


def rand_neuron():
    return np.random.randint(min_neuron_value, max_neuron_value+1)


def uniform():

    neurons_vals = {}

    for n in Synaptics.neurons:
        neurons_vals[n] = rand_neuron()

    return neurons_vals


def min_neurons():

    neurons_vals = {}

    for n in Synaptics.neurons:
        neurons_vals[n] = min_neuron_value

    return neurons_vals


def dist0(n0, zero_neurons):

    nneurons = len(Synaptics.neurons)

    active_neurons = [Synaptics.neurons[i]
                      for i in np.random.choice(nneurons, size=n0, replace=False)]

    neurons_vals = zero_neurons

    for n in active_neurons:
        # print(n, neurons_vals[n], neurons_vals[n] == min_neuron_value)
        if neurons_vals[n] == min_neuron_value:
            # print("activate", n)
            neurons_vals[n] = max_neuron_value
        else:
            # print("deactivate", n)
            neurons_vals[n] = min_neuron_value
        # print(neurons_vals[n])

    # assert sum(neurons_vals.values()) == \
    #        (nneurons - n0) * min_neuron_value + n0 * max_neuron_value

    return neurons_vals


def print_usage():
    print("Usage:", sys.argv[0],
          "<kind>",
          file=sys.stderr)


def main():

    # filename="small_perturb_random_neurons.csv"

    # np.random.seed(seed)

    argv = sys.argv[1:]

    if len(argv) == 0 or len(argv) > 3:
        print_usage()
        sys.exit(2)
    elif len(argv) == 3:
        fzero_neurons = argv[2]
        zero_neurons = read_zero_neurons(fzero_neurons)
    else:
        zero_neurons = min_neurons()

    kind = argv[0]

    if kind == "uniform":
        assert len(argv) == 1, "If uniform zero file doesn't make sense"
        neurons_vals = uniform()
    elif kind == "dist0":
        n0 = int(argv[1])
        neurons_vals = dist0(n0, zero_neurons)
    else:
        print_usage()
        sys.exit(2)

    write_csv_from_dicts(neurons_vals)


if __name__ == '__main__':
    main()
