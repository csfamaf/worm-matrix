import rclpy
from rclpy.executors import SingleThreadedExecutor
import threading

from .headed_sensor_controller import HeadedSensorController
from .headed_connectome import HeadedConnectome
from .sleeper import NodeSleeper


def main(args=None):

    rclpy.init(args=args)

    executor = SingleThreadedExecutor()

    sleeper = NodeSleeper(verbosity=True)

    executor.add_node(sleeper)
    thread = threading.Thread(target=executor.spin, args=(), daemon=True)
    thread.start()

    sleeper.wait_ready()

    connectome_worm = HeadedSensorController(HeadedConnectome, sleeper, verbosity=True)

    print('To finish the controller do:',
          'ros2 service call /worm/change_state lifecycle_msgs/srv/ChangeState '
          '"{transition: {id: 8}}"')

    try:
        # Run until Future by receiving "Destroy" with service
        rclpy.spin_until_future_complete(connectome_worm, connectome_worm.done)
    except KeyboardInterrupt:
        # Destroy the node explicitly
        # (optional - otherwise it will be done automatically
        # when the garbage collector destroys the node object)
        # connectome_worm.destroy_node()
        # rclpy.shutdown()
        print("Interrupted")
        # sys.exit()

    sleeper.destroy_node()
    connectome_worm.destroy_node()
    rclpy.shutdown()
    print("Done")


if __name__ == '__main__':
    main()
