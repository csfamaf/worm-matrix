import sys
import csv
import os.path
import argparse

import rclpy
from rclpy.node import Node
from gazebo_msgs.msg import ContactsState
from nav_msgs.msg import Odometry
from rclpy.qos import QoSHistoryPolicy, qos_profile_sensor_data
from builtin_interfaces.msg import Time
from asyncio import Future

MAX_TIME = 1800  # seconds


class MyTime(Time):

    NANO_IN_SEC = 10 ** 9

    def __init__(self, sec=0, nanosec=0, nano_step=5*(10**8), **kwargs):

        if sec < 0:
            raise ValueError('Seconds value must not be negative')
        if nanosec < 0:
            raise ValueError('Nanoseconds value must not be negative')

        super().__init__(sec=sec, nanosec=nanosec, **kwargs)

        self.nano_step = nano_step

    def __le__(self, other):

        if not isinstance(other, Time):
            raise TypeError("Can't compare times with different time types")

        return self.sec < other.sec or (self.sec == other.sec and self.nanosec <= other.nanosec)

    def __lt__(self, other):

        if not isinstance(other, Time):
            raise TypeError("Can't compare times with different time types")

        return self.sec < other.sec or (self.sec == other.sec and self.nanosec < other.nanosec)

    @classmethod
    def nano_to_sec_nano(cls, nanos: int):
        return nanos // cls.NANO_IN_SEC, nanos % cls.NANO_IN_SEC

    def set_ceil(self, other: Time, nano_step=None):

        if not isinstance(other, Time):
            raise TypeError("Can't compare times with different time types")

        if not nano_step:
            nano_step = self.nano_step

        other_nanos = other.sec * self.NANO_IN_SEC + other.nanosec

        nanos = ((other_nanos + nano_step - 1) // nano_step) * nano_step

        self.sec, self.nanosec = self.nano_to_sec_nano(nanos)

    def set_halfsec_ceil(self, other: Time):

        if not isinstance(other, Time):
            raise TypeError("Can't compare times with different time types")

        self.set_ceil(other, nano_step=5*(10**8))


class StatCollision(Node):

    def __init__(self, run, max_time, time_freq=2):

        super().__init__('stat_collision')

        self.run = run
        self.max_time = MyTime(sec=max_time)

        self.time_freq = time_freq

        self.values = {}
        self.ranges = range(run)
        self.last_range = 0

        qos_profile = qos_profile_sensor_data
        qos_profile.depth = 1
        qos_profile.history = QoSHistoryPolicy.RMW_QOS_POLICY_HISTORY_KEEP_LAST

        self.subscription_bumper = self.create_subscription(
            ContactsState,
            'worm/bumper_worm',
            self.listener_collision_callback,
            qos_profile)

        self.subscription_odom = self.create_subscription(
            Odometry,
            'worm/odom_worm',
            self.listener_odom_callback,
            qos_profile)

        self.done = Future()

        self.curr_time = MyTime(nano_step=round(MyTime.NANO_IN_SEC/self.time_freq))

        self.collisions = []
        self.odom = []

    def listener_collision_callback(self, msg: ContactsState):

        if not self.done.done():
            states = msg.states

            if states:  # list is not empty
                self.get_logger().info(str(states))
                time = msg.header.stamp
                self.collisions.append(
                    {'sec': time.sec,
                     'nanosec': time.nanosec,
                     }
                )

    def __add_odom_data(self, time, pos, orientation, twist):
        self.odom.append(
            {"run": self.run,
             "sec": self.curr_time.sec,
             "nanosec": self.curr_time.nanosec,
             "real_sec": time.sec,
             "real_nanosec": time.nanosec,
             "x": pos.x,
             "y": pos.y,
             "z": pos.z,
             "orientation_x": orientation.x,
             "orientation_y": orientation.y,
             "orientation_z": orientation.z,
             "orientation_w": orientation.w,
             "twist_linear_x": twist.linear.x,
             "twist_linear_y": twist.linear.y,
             "twist_linear_z": twist.linear.z,
             "twist_angular_x": twist.angular.x,
             "twist_angular_y": twist.angular.y,
             "twist_angular_z": twist.angular.z,
             }
        )

    def listener_odom_callback(self, msg: Odometry):

        if not self.done.done():

            time = msg.header.stamp

            if time > self.curr_time:
                self.__add_odom_data(
                    time, msg.pose.pose.position,
                    msg.pose.pose.orientation, msg.twist.twist)

            if self.curr_time >= self.max_time:
                self.done.set_result(True)
                print("Set done!!")

            self.curr_time.set_ceil(time)


def file_exist(fname: str):
    return os.path.isfile(fname)


def file_empty(fname: str):
    return os.stat(fname).st_size == 0


def convert_to_odom_file(fname: str):

    dirname = os.path.dirname(fname)
    basename = os.path.basename(fname)

    return os.path.join(dirname, "odom_" + basename)


def write_csv_from_dicts(filename, dicts):

    with open(filename, 'a', newline='') as csvfile:
        if len(dicts) > 0:
            fieldnames = list(dicts[0].keys())
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            if not file_exist(filename) or file_empty(filename):
                writer.writeheader()
            writer.writerows(dicts)


def main(args=None):

    rclpy.init(args=args)

    parser = argparse.ArgumentParser()

    parser.add_argument("nrun", type=int, help="Run number")
    parser.add_argument("collision_file", help="Collision data name file")
    # ROS2 args
    parser.add_argument("--ros-args", action="store_true", help="ROS argument")
    parser.add_argument("--params-file", help="ROS argument")
    parser.add_argument("-r", help="ROS argument")

    args = parser.parse_args()

    run = args.nrun

    collision_result_file = args.collision_file

    odom_result_file = convert_to_odom_file(collision_result_file)

    stat_collision = StatCollision(run, MAX_TIME)

    stat_collision.get_logger().info("Begin run {}, result: {}".format(run, collision_result_file))

    try:
        rclpy.spin_until_future_complete(stat_collision, stat_collision.done)
    except KeyboardInterrupt:
        # Destroy the node explicitly
        # (optional - otherwise it will be done automatically
        # when the garbage collector destroys the node object)
        # stat_collision.destroy_node()
        # rclpy.shutdown()
        stat_collision.get_logger().info("Interrupted run {}".format(run))
        stat_collision.destroy_node()
        sys.exit()

    stat_collision.get_logger().info("Write stats")

    write_csv_from_dicts(collision_result_file, stat_collision.collisions)
    write_csv_from_dicts(odom_result_file, stat_collision.odom)

    stat_collision.get_logger().info("Done run {}".format(run))
    stat_collision.destroy_node()

    rclpy.shutdown()


if __name__ == '__main__':

    main()
