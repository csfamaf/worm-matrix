from abc import ABC, abstractmethod
# from math import pi

from .sleeper import Sleeper

from typing import Tuple


class Robot(ABC):

    # GoPiGo3 has 1000 dps max motor speed
    # (https://forum.dexterindustries.com/t/maximum-dps-on-motor-for-gopigo3/5235)
    # With 0.0665m wheel diameter max. vel. is 0.55 m/s (255 set_speed parameter)
    max_dps = 1000.0
    # Above not used
    # Uses less according
    # https://forum.arduino.cc/index.php?action=dlattach;topic=293864.0;attach=110567
    # and https://hobbycomponents.com/motors-and-servos/ \
    # 124-smart-car-wheel-robot-plastic-dc-3v-6v-drive-gear-motor-with-tire-
    # then using 120 rpm = 720 dps
    # max_dps = 720.0

    wheel_diameter = 0.0665  # meters
    # max_speed = pi * wheel_diameter * max_dps / 360.0 # = 0.58 m/s
    max_speed = 1.388  # Pablo car measure (m/s)
    speed_factor = max_speed / 255
    # speed_factor = max_speed / 255 / 2
    wheel_separation = 0.11  # meters
    # max_angular_speed = 2.0 * max_speed / wheel_separation  # = 10,55 rad/s
    max_angular_speed = 6.7348  # rad/s
    angular_factor = max_angular_speed / max_speed

    def __init__(self, sleeper: Sleeper):
        self.sleeper = sleeper

        # For controller inspection. Set it inside Controller abstract class
        self.__controller = None

    @abstractmethod
    def set_speed(self, speed):
        pass

    @abstractmethod
    def fwd(self):
        pass

    @abstractmethod
    def bwd(self):
        pass

    @abstractmethod
    def left_rot(self):
        pass

    @abstractmethod
    def right_rot(self):
        pass

    @abstractmethod
    def stop(self):
        pass

    @abstractmethod
    def ds_read(self, msg):
        """Convert msg.Range to mm."""
        pass

    def time_sleep(self, secs: float):
        self.sleeper.sleep(secs)

    def get_time(self) -> Tuple[int, int]:
        """Return (second, nanoseconds)."""
        return self.sleeper.get_time()

    @property
    def controller(self):
        return self.__controller

    @controller.setter
    def controller(self, controller):
        self.__controller = controller
