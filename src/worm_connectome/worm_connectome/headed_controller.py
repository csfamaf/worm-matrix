from abc import ABC, abstractmethod
from .headed_robot import HeadedRobot


class HeadedController(ABC):

    @abstractmethod
    def __init__(self, robot: HeadedRobot, logger_getter, disembodied=False, verbosity=False,
                 **kwargs):

        if not isinstance(robot, HeadedRobot):
            raise TypeError("robot must be an instance of HeadedRobot")

        self.robot: HeadedRobot = robot
        self.robot.controller = self
        self.disembodied = disembodied
        self.verbosity = verbosity
        self.get_logger = logger_getter

    @abstractmethod
    def listener_callback(self, msg):
        pass
