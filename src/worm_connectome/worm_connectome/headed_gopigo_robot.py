from geometry_msgs.msg import Twist

from .sleeper import Sleeper
from .headed_robot import HeadedRobot
from .gopigo_robot import GoPiGoRobot

from math import pi


class Head:

    max_dps = HeadedRobot.max_dps
    max_angular_speed = max_dps * 2 * pi / 360
    angular_factor = max_angular_speed / 255

    def __init__(self, publisher, logger_getter, verbosity=0):

        self.motor = publisher
        self.get_logger = logger_getter
        self.verbosity = verbosity

        self.angular_speed = 0.0

        self.tw = Twist()
        self.zero_twist()

    def zero_twist(self):
        self.tw.linear.y = 0.0
        self.tw.linear.z = 0.0
        self.tw.angular.y = 0.0
        self.tw.angular.x = 0.0

        self.tw.linear.x = 0.0  # meter per second
        self.tw.angular.z = 0.0  # radians per second

    def send_command(self):
        self.motor.publish(self.tw)
        if self.verbosity:
            self.get_logger().info(
                "Head Twist x = {:.2f}".format(self.tw.angular.z)
            )


class HeadedGoPiGoRobot(GoPiGoRobot, HeadedRobot):

    def __init__(self, body_publisher, head_publisher, sleeper: Sleeper, logger_getter,
                 verbosity=0, speed=75):

        HeadedRobot.__init__(self, sleeper)
        GoPiGoRobot.__init__(self, body_publisher, sleeper, logger_getter, verbosity=verbosity,
                             speed=speed)

        self._head = Head(head_publisher, logger_getter, verbosity=verbosity)

        self.head_set_speed(0.0)

    def head_set_speed(self, speed):
        self._head.angular_speed = self._head.angular_factor * abs(speed)
        if self.verbosity > 2:
            self.get_logger().info(
                "Head Angular Speed (Controller, Simul) = ({},{:.2f})".format(
                    speed, self._head.angular_speed
                )
            )

    def head_left_rot(self):
        self._head.zero_twist()
        self._head.tw.angular.z = self._head.angular_speed
        self._head.send_command()

    def head_right_rot(self):
        self._head.zero_twist()
        self._head.tw.angular.z = -self._head.angular_speed
        self._head.send_command()

    def head_stop(self):
        self._head.zero_twist()
        self._head.send_command()
