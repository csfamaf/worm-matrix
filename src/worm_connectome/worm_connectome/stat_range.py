import sys
import math
import csv
import os.path

import rclpy
from rclpy.node import Node
from nav_msgs.msg import Odometry
from builtin_interfaces.msg import Time
from asyncio import Future

SAMPLING_RATE = 4


class MyTime(Time):

    NANO_IN_SEC = 10 ** 9

    def __init__(self, sec=0, nanosec=0, nano_step=5*(10**8), **kwargs):

        if sec < 0:
            raise ValueError('Seconds value must not be negative')
        if nanosec < 0:
            raise ValueError('Nanoseconds value must not be negative')

        super().__init__(sec=sec, nanosec=nanosec, **kwargs)

        self.nano_step = nano_step

    def __le__(self, other):

        if not isinstance(other, Time):
            raise TypeError("Can't compare times with different time types")

        return self.sec < other.sec or (self.sec == other.sec and self.nanosec <= other.nanosec)

    def __lt__(self, other):

        if not isinstance(other, Time):
            raise TypeError("Can't compare times with different time types")

        return self.sec < other.sec or (self.sec == other.sec and self.nanosec < other.nanosec)

    @classmethod
    def nano_to_sec_nano(cls, nanos: int):
        return nanos // cls.NANO_IN_SEC, nanos % cls.NANO_IN_SEC

    def set_ceil(self, other: Time, nano_step=None):

        if not isinstance(other, Time):
            raise TypeError("Can't compare times with different time types")

        if not nano_step:
            nano_step = self.nano_step

        other_nanos = other.sec * self.NANO_IN_SEC + other.nanosec

        nanos = ((other_nanos + nano_step - 1) // nano_step) * nano_step

        self.sec, self.nanosec = self.nano_to_sec_nano(nanos)

    def set_halfsec_ceil(self, other: Time):

        if not isinstance(other, Time):
            raise TypeError("Can't compare times with different time types")

        self.set_ceil(other, nano_step=5*(10**8))
        # if other.nanosec < 500000000:
        #     self.sec, self.nanosec = other.sec, 500000000
        # else:
        #     self.sec, self.nanosec = other.sec + 1, 0


class StatRange(Node):

    def __init__(self, run, ndist_steps=300, max_dist=30, max_time=None, time_freq=2):

        super().__init__('stat_range')

        # Stop by dist or time, not both
        if max_time:
            self.done_by_distance = False
            self.max_time = MyTime(sec=max_time)
        else:
            self.done_by_distance = True

        self.time_freq = time_freq

        self.max_dist = max_dist

        self.ndist_steps = ndist_steps

        self.run = run
        self.values = {}
        self.ranges = range(run)
        self.last_range = 0

        self.subscription = self.create_subscription(
            Odometry,
            'worm/odom_worm',
            self.listener_odom_callback,
            10)

        self.done = Future()

        # self.range2 = [(self.max_dist * ns / self.nsteps)**2 for ns in range(1, self.nsteps+1)]

        self.curr_dist_step = 0

        self.curr_time = MyTime(nano_step=round(MyTime.NANO_IN_SEC/self.time_freq))

        self.stats = []
        self.odom = []

    def range2(self, step):
        return (self.max_dist * (step+1) / self.ndist_steps)**2

    def listener_odom_callback(self, msg: Odometry):

        if not self.done.done():

            time = msg.header.stamp
            pos = msg.pose.pose.position
            # self.get_logger().info(str(time) + str(pos))

            dist2 = pos.x ** 2 + pos.y ** 2
            # self.get_logger().info(str(dist2))

            curr_range2 = self.range2(self.curr_dist_step)
            if dist2 >= curr_range2:
                self.stats.append(
                    {"run": self.run,
                     "sec": time.sec,
                     "nanosec": time.nanosec,
                     "step": self.curr_dist_step,
                     "dist": math.sqrt(curr_range2),
                     "x": pos.x,
                     "y": pos.y,
                     }
                )

                self.get_logger().info(str(self.stats[-1]))
                self.curr_dist_step += 1

                #  finish if last step
                if self.done_by_distance and self.curr_dist_step >= self.ndist_steps:
                    self.done.set_result(True)

            if time > self.curr_time:
                self.odom.append(
                    {"run": self.run,
                     "sec": self.curr_time.sec,
                     "nanosec": self.curr_time.nanosec,
                     "real_sec": time.sec,
                     "real_nanosec": time.nanosec,
                     "x": pos.x,
                     "y": pos.y,
                     "z": pos.z,
                     "orientation_x": msg.pose.pose.orientation.x,
                     "orientation_y": msg.pose.pose.orientation.y,
                     "orientation_z": msg.pose.pose.orientation.z,
                     "orientation_w": msg.pose.pose.orientation.w,
                     "twist_linear_x": msg.twist.twist.linear.x,
                     "twist_linear_y": msg.twist.twist.linear.y,
                     "twist_linear_z": msg.twist.twist.linear.z,
                     "twist_angular_x": msg.twist.twist.angular.x,
                     "twist_angular_y": msg.twist.twist.angular.y,
                     "twist_angular_z": msg.twist.twist.angular.z,
                     }
                )
                #  finish if last step
                if not self.done_by_distance and self.curr_time >= self.max_time:
                    self.done.set_result(True)

                self.curr_time.set_ceil(time)


def file_exist(fname: str):
    return os.path.isfile(fname)


def file_empty(fname: str):
    return os.stat(fname).st_size == 0


def write_csv_from_dicts(filename, dicts):

    with open(filename, 'a', newline='') as csvfile:
        fieldnames = list(dicts[0].keys())
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        if not file_exist(filename) or file_empty(filename):
            writer.writeheader()
        writer.writerows(dicts)


def fix_odom_init_time(odom):
    """Fix init time not zero."""
    sec, nano = odom[1]['sec'], odom[1]['nanosec']

    step = round(MyTime.NANO_IN_SEC / SAMPLING_RATE)
    if nano > 0:
        odom[0]['sec'] = sec
        odom[0]['nanosec'] = nano - step
    else:
        odom[0]['sec'] = sec - 1
        odom[0]['nanosec'] = MyTime.NANO_IN_SEC - step


def main(args=None):

    rclpy.init(args=args)

    run = int(sys.argv[1])

    stat_range_result_file = sys.argv[2]

    odom_result_file = sys.argv[3]

    stat_range = StatRange(run, max_time=3600, time_freq=SAMPLING_RATE)

    stat_range.get_logger().info("Begin run {}, result: {}".format(run, stat_range_result_file))

    try:
        rclpy.spin_until_future_complete(stat_range, stat_range.done)
    except KeyboardInterrupt:
        # Destroy the node explicitly
        # (optional - otherwise it will be done automatically
        # when the garbage collector destroys the node object)
        # stat_range.destroy_node()
        # rclpy.shutdown()
        stat_range.get_logger().info("Interrupted run {}".format(run))
        stat_range.destroy_node()
        sys.exit()

    stat_range.get_logger().info("Write stats")

    write_csv_from_dicts(stat_range_result_file, stat_range.stats)

    fix_odom_init_time(stat_range.odom)
    write_csv_from_dicts(odom_result_file, stat_range.odom)

    stat_range.get_logger().info("Done run {}".format(run))
    stat_range.destroy_node()

    rclpy.shutdown()


if __name__ == '__main__':

    main()
