from abc import ABC, abstractmethod
from .robot import Robot


class Controller(ABC):

    @abstractmethod
    def __init__(self, robot: Robot, logger_getter, disembodied=False, verbosity=False, **kwargs):

        if not isinstance(robot, Robot):
            raise TypeError("robot must be an instance of GoPiGoRobot")

        self.robot = robot
        self.robot.controller = self
        self.disembodied = disembodied
        self.verbosity = verbosity
        self.get_logger = logger_getter

    @abstractmethod
    def listener_callback(self, msg):
        pass
