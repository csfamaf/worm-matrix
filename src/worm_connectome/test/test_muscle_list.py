# Copyright 2015 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Some command line test with colcon:
# colcon test --packages-select worm_connectome --pytest-args -s test/test_muscle_list.py
# colcon test --packages-select worm_connectome --pytest-args -c worm_connectome/pytest.ini
# colcon test --packages-select worm_connectome --pytest-args " --ignore=./test/test_flake8.py"

from worm_connectome.synaptics import Synaptics

import pytest


# @pytest.fixture(params=["ady_matrix_timothy.json"])

@pytest.fixture(params=["ady_matrix_timothy.json", "hermaphrodite_chemical.json"])
def ady_matrix_file(request):
    return request.param


def mock_logger():

    class MockLogger:

        def __init__(self):
            pass

        def info(self, msg):
            print(str)

    return MockLogger()


@pytest.fixture
def syn(ady_matrix_file):
    """Return Synaptic object."""
    return Synaptics(ady_matrix_file, mock_logger)


def assert_muscle_prefix(muscle_prefixes, muscles):

    prefixes = tuple(muscle_prefixes)

    assert all(m.startswith(prefixes) for m in muscles), \
        "{} not start with {}".format(muscles, prefixes)


def test_muscle_prefix1(syn):

    muscles = syn.muscleBodyList \
              + syn.mBodyLeft + syn.mBodyRight \
              + syn.musBodyDleft + syn.musBodyDright \
              + syn.musBodyVleft + syn.musBodyVright \
              + syn.muscleHeadList + syn.mHeadLeft + syn.mHeadRight

    assert_muscle_prefix(syn.muscles, muscles)


def test_muscle_prefix_left(syn):

    left_muscles = syn.mBodyLeft + syn.musBodyDleft + syn.musBodyVleft + syn.mHeadLeft

    assert_muscle_prefix(syn.musLeftPrefix, left_muscles)


def test_muscle_prefix_right(syn):

    right_muscles = syn.mBodyRight + syn.musBodyDright + syn.musBodyVright + syn.mHeadRight

    assert_muscle_prefix(syn.musRightPrefix, right_muscles)


def test_muscle_prefix_head(syn):
    assert_muscle_prefix(syn.musLeftPrefix, syn.mHeadLeft)
    assert_muscle_prefix(syn.musRightPrefix, syn.mHeadRight)


def test_muscle_body(syn):
    assert sorted(syn.muscleBodyList) == sorted(syn.mBodyLeft + syn.mBodyRight), \
        "muscleBodyList is no equal to mBodyLeft + mBodyRight"


def assert_muscle_duplicated(muscles):
    assert len(muscles) == len(set(muscles))


def test_duplicates(syn):
    assert_muscle_duplicated(syn.muscleBodyList)
    assert_muscle_duplicated(syn.mBodyLeft)
    assert_muscle_duplicated(syn.mBodyRight)
    assert_muscle_duplicated(syn.musBodyDleft)
    assert_muscle_duplicated(syn.musBodyDright)
    assert_muscle_duplicated(syn.musBodyVleft)
    assert_muscle_duplicated(syn.musBodyVright)
    assert_muscle_duplicated(syn.muscleHeadList)
    assert_muscle_duplicated(syn.mHeadLeft)
    assert_muscle_duplicated(syn.mHeadRight)
    assert_muscle_duplicated(syn.cells)
    assert_muscle_duplicated(syn.neurons)


def test_dorsal_ventral_prefix(syn):
    assert_muscle_prefix(["MVL"], syn.musBodyVleft)
    assert_muscle_prefix(["MVR"], syn.musBodyVright)
    assert_muscle_prefix(["MDL"], syn.musBodyDleft)
    assert_muscle_prefix(["MDR"], syn.musBodyDright)


def test_dorsal_ventral_completness(syn):
    assert sorted(syn.mBodyLeft) == sorted(syn.musBodyVleft + syn.musBodyDleft)
    assert sorted(syn.mBodyRight) == sorted(syn.musBodyVright + syn.musBodyDright)


def test_cells_in_ady_matrix(syn):
    assert all(n in syn.neurons for n in syn.src_neurons), "Not all src neurons in syn.neurons:"

    tgt_cells = [c for tgts in syn.adyMatrix.values() for c, _ in tgts.items()]
    assert all(n in syn.cells for n in tgt_cells), "Not all tgt cells in syn.cells:"

    assert set(syn.src_neurons + tgt_cells) == set(
        syn.cells), "Synaptics.cells and ady matrix are not equal:"

    postSynapticNeurons = sorted(
        [n for n in syn.postSynaptic.keys() if n[:3] not in syn.musPrefix])

    assert all(n in postSynapticNeurons for n in syn.src_neurons), \
        "Cells not muscle in postSynaptics are not equal to src neurons in ady matrix"

    assert syn.neurons == postSynapticNeurons, \
        "Cells not muscle in postSynaptics are not equal to syn.neurons"


def test_nose_neurons(syn):
    src_neurons = syn.adyMatrix.keys()
    assert all(
        n in src_neurons for n in syn.nose_neurons), "Not all syn.nose_neurons in src neurons:"
    assert all(
        n in syn.neurons for n in syn.nose_neurons), "Not all syn.nose_neurons in neurons:"


def test_food_neurons(syn):
    src_neurons = syn.adyMatrix.keys()
    assert all(
        n in src_neurons for n in syn.food_neurons), "Not all syn.nose_neurons in src neurons:"
    assert all(
        n in syn.neurons for n in syn.food_neurons), "Not all syn.nose_neurons in neurons:"


def test_muscle_vulva(syn):
    assert not syn.musVulva or syn.musVulva == ['MVULVA']
