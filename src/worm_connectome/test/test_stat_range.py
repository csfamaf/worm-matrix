# Copyright 2015 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from worm_connectome.stat_range import MyTime
from builtin_interfaces.msg import Time

import pytest


@pytest.mark.parametrize(
    "sec, nanosec, step, expected_sec, expected_nanosec", [
        (5, 5 * (10**8) + 1,  5 * (10**8),  6, 0),
        (5, 0,                5 * (10**8),  5, 0),
        (5, 0,                25 * (10**7), 5, 0),
        (5, 75 * (10**7) - 1, 25 * (10**7), 5, 75 * (10**7)),
        (5, 75 * (10**7),     25 * (10**7), 5, 75 * (10**7)),
        (5, 75 * (10**7) + 1, 25 * (10**7), 6, 0),
    ])
def test_ceil(sec, nanosec, step, expected_sec, expected_nanosec):

    t = Time(sec=sec, nanosec=nanosec)

    tm = MyTime(nano_step=step)

    tm.set_ceil(t)

    assert tm.sec == expected_sec and tm.nanosec == expected_nanosec


def test_halfceils():

    nanos = 5 * (10**8) + 1

    t = Time(sec=5, nanosec=nanos)

    tm = MyTime()

    tm.set_halfsec_ceil(t)

    assert tm.sec == 6 and tm.nanosec == 0
