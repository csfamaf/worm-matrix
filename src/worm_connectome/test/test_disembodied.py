# Copyright 2015 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Some command line test with colcon:
# colcon test --packages-select worm_connectome --pytest-args -s test/test_muscle_list.py
# colcon test --packages-select worm_connectome --pytest-args -c worm_connectome/pytest.ini
# colcon test --packages-select worm_connectome --pytest-args " --ignore=./test/test_flake8.py"

from worm_connectome.run_disembodied import run_disembodied

import pytest
import os
import filecmp


# @pytest.fixture(params=[("ady_matrix_timothy.json", "cells_out_timothy.csv")])


@pytest.fixture(
    params=[("ady_matrix_timothy.json", "cells_out_timothy.csv"),
            ("hermaphrodite_chemical.json", "cells_out_cook_hermaphrodite_chemical.csv")])
def ady_matrix_file_out(request):
    return request.param


def test_connectome_out(tmpdir, ady_matrix_file_out):

    ady_matrix_file, file_cells_test = ady_matrix_file_out

    file_path_name = tmpdir.join("path.out")
    file_out_cells = tmpdir.join("cells.out")

    run_disembodied(file_path_name, file_out_cells, ady_matrix_file=ady_matrix_file,
                    max_time_sec=200)

    file_cells_test = os.path.join("test", "resources", file_cells_test)
    assert filecmp.cmp(file_out_cells, file_cells_test, shallow=False)
