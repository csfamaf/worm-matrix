from os import path
from setuptools import setup
from glob import glob

package_name = 'worm_connectome'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (path.join('share', package_name), glob('launch/*.launch.py')),
        (path.join('share', package_name, 'worlds'), glob('../../worlds/*')),
        (path.join('share', package_name, 'files'), glob('files/*')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='damian',
    maintainer_email='damian@famaf.unc.edu.ar',
    description='Move robot with connectome. Also, with teleop for testing',
    license='Apache License 2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'teleop = worm_connectome.teleop:main',
            'connectome = worm_connectome.run_connectome:main',
            'connectome_emmons = worm_connectome.run_connectome_emmons:main',
            'disembodied = worm_connectome.run_disembodied:main',
            'random_blind = worm_connectome.run_random_blind:main',
            'stat_range = worm_connectome.stat_range:main',
            'stat_maze = worm_connectome.stat_maze:main',
            'stat_collision = worm_connectome.stat_collision:main',
            'headed_teleop = worm_connectome.headed_teleop:main',
            'headed_connectome = worm_connectome.headed_run_connectome:main',
        ],
    },
)
