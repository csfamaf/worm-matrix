from setuptools import setup

from os.path import basename
from glob import glob

package_name = 'robot_spawner_pkg'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        ] + [('share/' + package_name + '/models/' + basename(i), glob(i + '/*')) for i in glob('../../models/*')],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='damian',
    maintainer_email='damian@famaf.unc.edu.ar',
    description='Spawn robot in gazebo with message service',
    license='Apache License 2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'spawn_worm = robot_spawner_pkg.spawn_worm:main',
        ],
    },
)
