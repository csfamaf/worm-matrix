/*
 * Copyright (C) 2017 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include <gazebo/gazebo.hh>
#include <gazebo/physics/Joint.hh>
#include <gazebo/physics/JointController.hh>
#include <gazebo/physics/Model.hh>
#include <gazebo/physics/PhysicsTypes.hh>

#include <geometry_msgs/msg/twist.hpp>

namespace gazebo
{
  //////////////////////////////////////////////////
  /// \brief Sets velocity on a link or joint
  class SetJointVelocityPlugin : public ModelPlugin
  {

    public:

    physics::JointControllerPtr jointController;

    /// Protect variables accessed on callbacks.
    std::mutex lock_;

    /// Linear velocity in X received on command (m/s).
    double target_x_{0.0};
    /// Angular velocity in Z received on command (rad/s).
    double target_rot_{0.0};



    virtual void Load(physics::ModelPtr _model, [[maybe_unused]]sdf::ElementPtr _sdf)
      {
        this->model = _model;
        this->updateConnection = event::Events::ConnectWorldUpdateBegin(
        std::bind(&SetJointVelocityPlugin::Update, this, std::placeholders::_1));
      }

    void Update([[maybe_unused]]const common::UpdateInfo &_info)
      {
//         this->model->GetJoint("left_wheel_joint")->SetVelocity(0, 1.0);
//         this->model->GetJoint("right_wheel_joint")->SetVelocity(0, 0.0);
          // Joint velocity using joint motors
          this->model->GetJoint("left_wheel_joint")->SetParam("fmax", 0, 100.0);
          this->model->GetJoint("left_wheel_joint")->SetParam("vel", 0, 1.0);
          this->model->GetJoint("right_wheel_joint")->SetParam("fmax", 0, 100.0);
          this->model->GetJoint("right_wheel_joint")->SetParam("vel", 0, 0.0);

/*
        if (update_num == 0)
        {
          // Joint velocity instantaneously without applying forces
          this->model->GetJoint("gray_joint")->SetVelocity(0, 1.0);

          // Joint velocity using joint motors
          this->model->GetJoint("orange_joint")->SetParam("fmax", 0, 100.0);
          this->model->GetJoint("orange_joint")->SetParam("vel", 0, 1.0);

          // Joint velocity using PID controller
          this->jointController.reset(new physics::JointController(
                this->model));
          this->jointController->AddJoint(model->GetJoint("purple_joint"));
          std::string name = model->GetJoint("purple_joint")->GetScopedName();
          this->jointController->SetVelocityPID(name, common::PID(100, 0, 0));
          this->jointController->SetVelocityTarget(name, 1.0);
        }
        else if (update_num < 200)
        {
          // Must update PID controllers so they apply forces
          this->jointController->Update();
        }
        else if (update_num == 200)
        {
          // Joint motors are disabled by setting max force to zero
          this->model->GetJoint("orange_joint")->SetParam("fmax", 0, 0.0);
        }
        update_num++;
*/
      }

    /// \brief a pointer to the model this plugin was loaded by
    physics::ModelPtr model;
    /// \brief object for callback connection
    event::ConnectionPtr updateConnection;
    /// \brief number of updates received
    int update_num = 0;

    /// Callback when a velocity command is received.
    /// \param[in] _msg Twist command message.
  void OnCmdVel(const geometry_msgs::msg::Twist::SharedPtr _msg)
  {
    std::lock_guard<std::mutex> scoped_lock(lock_);
    target_x_ = _msg->linear.x;
    target_rot_ = _msg->angular.z;
  }

  };


  GZ_REGISTER_MODEL_PLUGIN(SetJointVelocityPlugin)
}
